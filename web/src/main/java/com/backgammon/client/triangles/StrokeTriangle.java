package com.backgammon.client.triangles;

import com.backgammon.common.game.draw.triangles.BGraphicsDecorator;
import com.google.gwt.canvas.dom.client.Context2d;

public class StrokeTriangle implements BGraphicsDecorator {
	private final Context2d ctx;
	private final String color;

	public StrokeTriangle(Context2d ctx, String color) {
		this.ctx = ctx;
		this.color = color;
	}

	public void setColor() {
		ctx.setStrokeStyle(color);
	}

	@Override
	public void decorate() {
		setColor();
		ctx.setLineWidth(2);
		ctx.stroke();
	}
}
