package com.backgammon.client.triangles;

import com.backgammon.common.game.draw.triangles.BGraphicsDecorator;
import com.google.gwt.canvas.dom.client.Context2d;

public class FullTriangle implements BGraphicsDecorator {
	private final Context2d ctx;
	private final String color;

	public FullTriangle(Context2d ctx, String color) {
		this.ctx = ctx;
		this.color = color;
	}

	@Override
	public void setColor() {
		ctx.setFillStyle(color);
	}

	@Override
	public void decorate() {
		ctx.fill();
	}
}
