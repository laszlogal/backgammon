package com.backgammon.client;

import com.backgammon.common.game.Game;
import com.google.gwt.core.client.EntryPoint;

public class WebBackgammon implements EntryPoint {

    /**
     * Entry point of the web backgammon game.
     */
    public void onModuleLoad() {
        Game game = new Game(new GameFactoryW());
        game.start();
    }
}
