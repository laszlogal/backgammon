package com.backgammon.client;

import java.util.HashMap;
import java.util.Map;

import com.backgammon.common.game.draw.BoardMetrics;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public class MultiCanvas implements IsWidget {
	public static final String BACKGROUND = "background";
	public static final String FOREGROUND = "foreground";
	public static final String EVENT = "event";
	public static final String ANIMATION = "animation";
	private Map<String, Canvas> canvases;
	private final FlowPanel main;
	private final BoardMetrics metrics;

	public MultiCanvas(BoardMetrics metrics) {
		this.metrics = metrics;
		main = new FlowPanel();
		addDefaultCanvases();
	}

	private void addDefaultCanvases() {
		canvases = new HashMap<>();
		addCanvas(BACKGROUND);
		addCanvas(ANIMATION);
		addCanvas(FOREGROUND);
		addEventCanvas();
	}

	private void addEventCanvas() {
		addCanvas(EVENT);
		Canvas canvas = getCanvas(EVENT);
		canvas.sinkEvents(Event.MOUSEEVENTS);
		canvas.getElement().getStyle().setZIndex(canvases.size() + 1);
	}

	public Context2d getForegroundContext2d() {
		return getCanvas(FOREGROUND).getContext2d();
	}

	public Context2d getBackgroundContext2d() {
		return getCanvas(BACKGROUND).getContext2d();
	}

	public Context2d getAnimationContext2d() {
		return getCanvas(ANIMATION).getContext2d();
	}

	private Canvas getCanvas(String key) {
		return canvases.get(key);
	}

	private void addCanvas(String name) {
		Canvas canvas = Canvas.createIfSupported();
		canvas.setStyleName(name);
		main.add(canvas);
		resize(canvas);
		canvases.put(name, canvas);
	}

	public void resize() {
		canvases.values().forEach(this::resize);
	}

	private void resize(Canvas canvas) {
		canvas.setCoordinateSpaceWidth(metrics.getWidth());
		canvas.setCoordinateSpaceHeight(metrics.getHeight());
	}

	public void addEventHandler(BoardEventHandlerW handler) {
		getCanvas(EVENT).addMouseDownHandler(handler);
		getCanvas(EVENT).addMouseMoveHandler(handler);
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	public void clear() {
		resize();
	}

	public void clear(String canvasId) {
		resize(canvases.get(canvasId));
	}
}
