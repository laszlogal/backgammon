package com.backgammon.client.bgcanvas;

import com.backgammon.common.bgraphics.BGraphics;
import com.google.gwt.canvas.dom.client.Context2d;

public class BGraphicsW implements BGraphics {
	private final Context2d ctx;

	public BGraphicsW(Context2d ctx) {

		this.ctx = ctx;
	}

	@Override
	public void setFillColor(String color) {
		ctx.setFillStyle(color);
	}

	@Override
	public void setLineColor(String color) {
		ctx.setStrokeStyle(color);
	}


	@Override
	public void fillRect(int left, int top, int width, int height) {
		ctx.fillRect(left, top, width, height);
	}

	@Override
	public void setLineWidth(int lineWidth) {
		ctx.setLineWidth(lineWidth);
	}

	@Override
	public void strokeRect(int left, int top, int width, int height) {
		beginPath();
		ctx.rect(left, top, width, height);
		closePath();
		ctx.stroke();
	}

	@Override
	public void drawLine(int x, int y, int x1, int y1) {
		ctx.beginPath();
		ctx.moveTo(x, y);
		ctx.lineTo(x1, y1);
		ctx.stroke();
		ctx.closePath();
	}

	@Override
	public void fillCircle(int x, int y, int radius) {
		ctx.beginPath();
		ctx.arc(x, y, radius, 0, 360, false);
		ctx.fill();
		ctx.closePath();
	}

	public void beginPath() {
		ctx.beginPath();
	}

	@Override
	public void closePath() {
		ctx.closePath();
	}

	@Override
	public void moveTo(int x, int y) {
		ctx.moveTo(x, y);
	}

	@Override
	public void lineTo(int x, int y) {
		ctx.lineTo(x, y);
	}

	@Override
	public void save() {
		ctx.save();
	}

	@Override
	public void restore() {
		ctx.restore();
	}

	@Override
	public void clear(int x, int y, int width, int height) {
		ctx.clearRect(x, y, width, height);
	}
}
