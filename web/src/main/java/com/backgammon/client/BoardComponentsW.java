package com.backgammon.client;

import com.backgammon.client.animation.StepLoopW;
import com.backgammon.client.bgcanvas.BGraphicsW;
import com.backgammon.client.triangles.FullTriangle;
import com.backgammon.client.triangles.StrokeTriangle;
import com.backgammon.common.game.board.BoardComponents;
import com.backgammon.common.game.board.BoardEventHandler;
import com.backgammon.common.game.draw.BoardMetrics;
import com.backgammon.common.game.draw.DrawBackground;
import com.backgammon.common.game.draw.DrawChecker;
import com.backgammon.common.game.draw.DrawCheckers;
import com.backgammon.common.game.draw.DrawDices;
import com.backgammon.common.game.draw.MoveAnimation;
import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.draw.triangles.DrawPointTriangles;
import com.backgammon.common.game.draw.triangles.PointDecorators;
import com.google.gwt.canvas.dom.client.Context2d;

public class BoardComponentsW implements BoardComponents {
	private final BoardMetrics metrics;
	private final MultiCanvas multiCanvas;
	private final DrawBackground background;
	private final DrawPointTriangles drawPointTriangles;
	private final DrawCheckers checkers;
	private final DrawDices dices;
	private final BoardEventHandlerW eventHandler;
	private final StepLoop stepLoop;

	public BoardComponentsW(int width, int height) {
		metrics = new BoardMetrics(width, height);
		metrics.setBoardBorderWidth(24);
		metrics.setGoalBorderWidth(12);
		multiCanvas = new MultiCanvas(metrics);
		Context2d bgContext = multiCanvas.getBackgroundContext2d();
		BGraphicsW bgGraphics = new BGraphicsW(bgContext);
		BGraphicsW fgGraphics = new BGraphicsW(multiCanvas.getForegroundContext2d());
		background = new DrawBackground(bgGraphics);
		PointDecorators decorators = new PointDecorators(new FullTriangle(bgContext, "brown"),
				new StrokeTriangle(bgContext, "brown"),
				new StrokeTriangle(bgContext, "yellow")
		);

		drawPointTriangles = new DrawPointTriangles(metrics, bgGraphics, decorators);
		DrawChecker drawChecker = new DrawChecker(metrics.triangleMiddle(), metrics.getCheckerRadius());
		checkers = new DrawCheckers(metrics, drawChecker, fgGraphics);
		dices = new DrawDices(metrics, fgGraphics);
		BGraphicsW animGraphics = new BGraphicsW(multiCanvas.getAnimationContext2d());
		MoveAnimation moveAnimation = new MoveAnimation(animGraphics, checkers, drawChecker);
		stepLoop = new StepLoopW(moveAnimation);
		eventHandler = new BoardEventHandlerW();
		multiCanvas.addEventHandler(eventHandler);
	}


	@Override
	public BoardMetrics getMetrics() {
		return metrics;
	}

	public MultiCanvas getMultiCanvas() {
		return multiCanvas;
	}

	@Override
	public DrawBackground getBackground() {
		return background;
	}

	@Override
	public DrawPointTriangles getDrawPointTriangles() {
		return drawPointTriangles;
	}

	@Override
	public DrawCheckers getCheckers() {
		return checkers;
	}

	@Override
	public DrawDices getDices() {
		return dices;
	}

	@Override
	public StepLoop getStepLoop() {
		return stepLoop;
	}

	public BoardEventHandler getEventHandler() {
		return eventHandler;
	}
}
