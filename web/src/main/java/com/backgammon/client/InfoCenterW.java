package com.backgammon.client;

import com.backgammon.common.game.InfoCenter;

public class InfoCenterW implements InfoCenter {
	private final HeaderWidget header;

	public InfoCenterW(HeaderWidget header) {
		this.header = header;
	}

	@Override
	public void win(String name) {
		setText(name + " wins! ");
	}

	@Override
	public void setText(String text) {
		header.info(text);
	}


}
