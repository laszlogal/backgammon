package com.backgammon.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

public class HeaderWidget extends Composite {

    private final Label label;

    public HeaderWidget() {
        super();
        FlowPanel main = new FlowPanel();
        main.addStyleName("gameHeader");
        label = new Label();
        main.add(label);
        initWidget(main);
    }

    public void info(String text) {
        label.setText(text);
    }
}
