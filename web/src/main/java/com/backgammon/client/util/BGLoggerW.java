package com.backgammon.client.util;

import com.backgammon.common.util.BGLogger;

public class BGLoggerW implements BGLogger {

	@Override
	public native void debug(String message) /*-{
		$wnd.console.log(message);
	}-*/;

	@Override
	public void info(String message) {
		debug(message);
	}
}
