package com.backgammon.client;

import com.backgammon.common.game.board.BoardEventHandler;
import com.backgammon.common.game.player.agent.HumanAgent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseEvent;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;

public class BoardEventHandlerW implements MouseDownHandler, MouseMoveHandler, BoardEventHandler {
	private HumanAgent agent;

	@Override
	public void onMouseDown(MouseDownEvent event) {
		agent.process(getRelativeX(event),
					getRelativeY(event));
	}

	private int getRelativeX(MouseEvent<?> event) {
		return event.getRelativeX(event.getRelativeElement());
	}

	private int getRelativeY(MouseEvent<?> event) {
		return event.getRelativeY(event.getRelativeElement());
	}

	@Override
	public void setAgent(HumanAgent agent) {
		this.agent = agent;
	}

	@Override
	public void onMouseMove(MouseMoveEvent event) {
		//
	}
}
