package com.backgammon.client.animation;

import com.backgammon.common.game.draw.MoveAnimation;
import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;

public class StepLoopW implements StepLoop {
	private static final int MOVE_SPEED = 2;
	public static final int PAUSE_BETWEEN_STEPS = 10;
	private final LoopW loop;
	private final MoveAnimation step;
	private MoveFinishedHandler handler;
	private int pauseCounter;
	private MoveChecker move;

	public StepLoopW(MoveAnimation step) {
		this.step = step;
		loop = new LoopW(this, MOVE_SPEED);
	}

	@Override
	public void setMove(MoveChecker move) {
		this.move = move;
		step.setMove(move);
	}

	@Override
	public void start() {
		step.start();
		pauseCounter = PAUSE_BETWEEN_STEPS;
		loop.start();
	}

	@Override
	public void run() {
		if (step.isFinished()) {
			pauseCounter--;
			if (pauseCounter == 0) {
				loop.stop();
			}
		} else {
			step.tick();
		}
	}

	@Override
	public void onEnd() {
		handler.onMoveFinished(move);
	}

	@Override
	public void setHandler(MoveFinishedHandler handler) {
		this.handler = handler;
	}
}
