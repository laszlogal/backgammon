package com.backgammon.client.animation;

import com.backgammon.common.bgraphics.Loop;
import com.backgammon.common.game.draw.AnimListener;
import com.backgammon.common.game.draw.ShuffleDice;

public class ShuffleDiceAnimation implements AnimListener {
	public static final int SPEED = 2;
	public static final int MAX_FRAMES = SPEED * 20;
	private final Loop loop;
	private final ShuffleDice shuffleDice;
	private int frameCounter;

	public ShuffleDiceAnimation(ShuffleDice shuffleDice) {
		this.shuffleDice = shuffleDice;
		loop = new LoopW(this, SPEED, MAX_FRAMES);
	}

	public void start() {
		frameCounter = 0;
		loop.start();
	}

	@Override
	public void run() {
		frameCounter++;
		if (frameCounter % SPEED == 0) {
			shuffleDice.next();
			frameCounter = 0;
		}
	}

	@Override
	public void onEnd() {
		shuffleDice.processResult();
	}
}
