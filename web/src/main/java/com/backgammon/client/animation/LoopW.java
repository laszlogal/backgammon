package com.backgammon.client.animation;

import com.backgammon.common.bgraphics.Loop;
import com.backgammon.common.game.draw.AnimListener;
import com.google.gwt.animation.client.AnimationScheduler;

public class LoopW extends Loop implements AnimationScheduler.AnimationCallback {
	public LoopW(AnimListener listener, int speed) {
		this(listener, speed, -1);
	}

	public LoopW(AnimListener listener, int speed, int maxFrames) {
		super(listener, speed, maxFrames);
	}

	@Override
	public void scheduleAnimation() {
		AnimationScheduler.get().requestAnimationFrame(this);
	}

	@Override
	public void execute(double timestamp) {
		execute();
	}
}
