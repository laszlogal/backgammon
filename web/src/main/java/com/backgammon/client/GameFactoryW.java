package com.backgammon.client;

import com.backgammon.client.animation.ShuffleDiceAnimation;
import com.backgammon.client.util.BGLoggerW;
import com.backgammon.common.game.GameFactory;
import com.backgammon.common.game.InfoCenter;
import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.BoardViewImpl;
import com.backgammon.common.game.board.point.BoardController;
import com.backgammon.common.game.board.point.BoardControllerImpl;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.ShuffleDice;
import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.agent.HumanAgent;
import com.backgammon.common.game.player.agent.RandomMovesAgent;
import com.backgammon.common.util.BGLogger;

public class GameFactoryW implements GameFactory {
	private BoardComponentsW components;
	private BackgammonPanel panel;
	private DrawBoardW drawBoardW;
	private ShuffleDiceAnimation diceAnimation;
	private final HeaderWidget header = new HeaderWidget();

	@Override
	public void createGui(BoardView view) {
		panel = new BackgammonPanel(view);
		panel.setHeader(header);
		panel.add(drawBoardW);
	}

	@Override
	public BGLogger createLogger() {
		return new BGLoggerW();
	}

	@Override
	public BoardView createView(Board board, DrawBoard drawBoard) {
		return new BoardViewImpl(board, drawBoard, components.getStepLoop());
	}

	@Override
	public void createDiceAnimation(ShuffleDice shuffleDice) {
		diceAnimation = new ShuffleDiceAnimation(shuffleDice);
	}

	@Override
	public DrawBoard createDrawBoard() {
		components = new BoardComponentsW(850, 650);
		drawBoardW = new DrawBoardW(components);
		drawBoardW.addStyleName("board");
		return drawBoardW;
	}

	@Override
	public void onGameStart() {
		panel.fitToScreenDeferred();
	}

	@Override
	public Runnable diceStart() {
		return diceAnimation::start;
	}

	@Override
	public BackgammonAgent playerAgent1() {
		return new RandomMovesAgent();
	}

	@Override
	public BackgammonAgent playerAgent2() {
		return new HumanAgent(createBoardController(), components.getEventHandler());
	}

	@Override
	public BoardController createBoardController() {
		return new BoardControllerImpl(components.getDrawPointTriangles());
	}

	@Override
	public InfoCenter infoCenter() {
		return new InfoCenterW(header);
	}
}
