package com.backgammon.client;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.CheckerSet;
import com.backgammon.common.game.draw.*;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;


public class DrawBoardW implements IsWidget, DrawBoard {
	public static final int MIN_BOARD_WIDTH = 300;

	private static final double BOARD_RATIO = 0.8;
	private final BoardMetrics metrics;
	private final MultiCanvas multiCanvas;
	private final DrawBoardCommon common;

	public DrawBoardW(BoardComponentsW parts) {
		common = new DrawBoardCommon(parts);
		metrics = parts.getMetrics();
		multiCanvas = parts.getMultiCanvas();
	}

	@Override
	public void drawBackground() {
		multiCanvas.clear();
		common.drawBackground();
	}

	@Override
	public void drawPoint(Point point) {
		common.drawPoint(point);
	}

	@Override
	public void drawGoal(CheckerSet goal) {
		common.drawGoal(goal);
	}

	@Override
	public void drawBar(Bar bar) {
		common.drawBar(bar);
	}

	@Override
	public void drawDices(int value1, int value2) {
		common.drawDices(value1, value2);
	}


	@Override
	public void resize(int width, int height) {
		int scaledHeight = (int) Math.round(width * BOARD_RATIO);
		if (width > MIN_BOARD_WIDTH && scaledHeight < height) {
			common.resize(width, scaledHeight);
			multiCanvas.resize();
		}

		center(width, height);
	}

	void center(int width, int height) {
		Style style = getParent().getElement().getStyle();
		style.setTop(centerValue(height, metrics.getHeight()), Style.Unit.PX);
		style.setLeft(centerValue(width, metrics.getWidth()), Style.Unit.PX);
	}


	private UIObject getParent() {
		return asWidget().getParent();
	}

	private double centerValue(double newValue, double oldValue) {
		return (newValue - oldValue) / 2;
	}

	@Override
	public Widget asWidget() {
		return multiCanvas.asWidget();
	}

	public void addStyleName(String styleName) {
		asWidget().addStyleName(styleName);
	}
}
