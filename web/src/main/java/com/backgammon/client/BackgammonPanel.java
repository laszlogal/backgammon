package com.backgammon.client;

import com.backgammon.common.game.board.BoardView;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class BackgammonPanel extends Composite implements ResizeHandler {

	private final BoardView view;
	private final FlowPanel main;
	public BackgammonPanel(BoardView view) {
		this.view = view;
		main = new FlowPanel();
		initWidget(main);
		addStyleName("main");
		RootPanel.get().add(this);
		Window.addResizeHandler(this);
	}

	public void fitToScreenDeferred() {
		Scheduler.get().scheduleDeferred(this::fitToScreen);
	}

	@Override
	public void onResize(ResizeEvent event) {
		fitToScreen();
	}

	private void fitToScreen() {
		view.resize(Window.getClientWidth(), Window.getClientHeight());
	}

	public void setHeader(HeaderWidget header) {
		RootPanel.get().add(header);
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	public void add(IsWidget widget) {
		main.add(widget);
	}
}
