package com.backgammon;

import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;

public class StepLoopA implements StepLoop {
    @Override
    public void setMove(MoveChecker move) {

    }

    @Override
    public void start() {

    }

    @Override
    public void setHandler(MoveFinishedHandler handler) {

    }

    @Override
    public void run() {

    }

    @Override
    public void onEnd() {

    }
}
