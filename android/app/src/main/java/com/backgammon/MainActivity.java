package com.backgammon;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import com.backgammon.game.GameA;
import com.backgammon.game.GameFactoryA;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BackgammonViewA androidView = findViewById(R.id.board);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        GameFactoryA factory = new GameFactoryA(androidView, size.x, size.y);
        GameA game = new GameA(factory);
        game.start();
    }

}
