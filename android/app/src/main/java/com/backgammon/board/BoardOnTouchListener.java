package com.backgammon.board;

import android.view.MotionEvent;
import android.view.View;

import com.backgammon.common.game.board.BoardEventHandler;
import com.backgammon.common.game.player.agent.HumanAgent;
import com.backgammon.common.util.Log;

public class BoardOnTouchListener implements View.OnTouchListener, BoardEventHandler {

    private HumanAgent agent;
    private View view;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        this.view = v;
        if (!agent.isInTurn()) {
            Log.debug("Not your turn");
        } else {
            agent.process((int) event.getX(), (int) event.getY());
            view.invalidate();
        }
        return false;
    }

    @Override
    public void setAgent(HumanAgent agent) {
        this.agent = agent;
    }
}
