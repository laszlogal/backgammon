package com.backgammon.board;

import android.graphics.Canvas;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.BoardViewImpl;
import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.Roll;
import com.backgammon.draw.DrawBoardA;

public class BoardViewA implements BoardView {

    private final BoardView impl;
    private DrawBoardA drawBoardA;

    public BoardViewA(Board board, DrawBoardA drawBoard, StepLoop stepLoop) {
        drawBoardA = drawBoard;
        impl = new BoardViewImpl(board, drawBoard, stepLoop);
    }

    @Override
    public void update() {
        impl.update();
    }

    public void update(Canvas canvas) {
        drawBoardA.update(canvas);
        impl.update();
    }

    @Override
    public void resize(int width, int height) {
        impl.resize(width, height);

    }

    @Override
    public void animateMove(MoveChecker move, MoveFinishedHandler handler) {
        impl.animateMove(move, handler);
    }

    @Override
    public void setRoll(Roll roll) {
        impl.setRoll(roll);
    }


    @Override
    public void updateDices(Roll roll) {
        impl.updateDices(roll);
    }

}
