package com.backgammon.board;

import com.backgammon.StepLoopA;
import com.backgammon.common.game.board.BoardComponents;
import com.backgammon.common.game.draw.BoardMetrics;
import com.backgammon.common.game.draw.DrawBackground;
import com.backgammon.common.game.draw.DrawChecker;
import com.backgammon.common.game.draw.DrawCheckers;
import com.backgammon.common.game.draw.DrawDices;
import com.backgammon.common.game.draw.MoveAnimation;
import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.draw.triangles.DrawPointTriangles;
import com.backgammon.common.game.draw.triangles.PointDecorators;
import com.backgammon.draw.BGraphicsA;
import com.backgammon.draw.EvenPointDecorator;
import com.backgammon.draw.OddPointDecorator;
import com.backgammon.draw.SelectedPointDecorator;

public class BoardComponentsA implements BoardComponents {

    private final BGraphicsA fgGraphics;
    private final BGraphicsA bgGraphics;
    private final DrawBackground background;
    private final DrawPointTriangles triangles;
    private final DrawCheckers checkers;
    private final DrawDices dices;
    private final DrawChecker drawChecker;
    private final MoveAnimation moveAnimation;
    private final BoardMetrics metrics;
    private final BoardOnTouchListener boardOnTouchListener;
    private final LayeredGraphics layers;

    public BoardComponentsA(int width, int height) {
        this.metrics = new BoardMetrics(800, 600);
        metrics.setBoardBorderWidth(24);
        metrics.setGoalBorderWidth(12);
        layers = new LayeredGraphics(width, height);
        bgGraphics = layers.background();
        fgGraphics = layers.foreground();
        background = new DrawBackground(bgGraphics);
        PointDecorators decorators = new PointDecorators(
                new OddPointDecorator(bgGraphics),
                new EvenPointDecorator(bgGraphics),
                new SelectedPointDecorator(bgGraphics));

        triangles = new DrawPointTriangles(metrics, bgGraphics, decorators);

        drawChecker = new DrawChecker(metrics.triangleMiddle(), metrics.getCheckerRadius());
        checkers = new DrawCheckers(metrics, drawChecker, fgGraphics);
        dices = new DrawDices(metrics, fgGraphics);
        moveAnimation = new MoveAnimation(fgGraphics, checkers, drawChecker);
        boardOnTouchListener  = new BoardOnTouchListener();
    }

    @Override
    public BoardMetrics getMetrics() {
        return metrics;
    }

    @Override
    public DrawBackground getBackground() {
        return background;
    }

    @Override
    public DrawPointTriangles getDrawPointTriangles() {
        return triangles;
    }

    @Override
    public DrawCheckers getCheckers() {
        return checkers;
    }

    @Override
    public DrawDices getDices() {
        return dices;
    }

    @Override
    public StepLoop getStepLoop() {
        return new StepLoopA();
    }

    public BoardOnTouchListener getBoardOnTouchListener() {
        return boardOnTouchListener;
    }

    public LayeredGraphics getLayers() {
        return layers;
    }
}
