package com.backgammon.board;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import com.backgammon.draw.BGraphicsA;

import java.util.ArrayList;
import java.util.List;

public class LayeredGraphics {
    public static final int FOREGROUND_INDEX = 1;
    public static final int BACKGROUND_INDEX = 0;
    private final int width;
    private final int height;
    List<BGraphicsA> graphics;

    public LayeredGraphics(int width, int height) {
        this.width = width;
        this.height = height;
        this.graphics = new ArrayList<>();
        addLayer();
        addLayer();
    }

    private void addLayer() {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        graphics.add(new BGraphicsA(new Paint(), bitmap));
    }

    public BGraphicsA foreground() {
        return graphics.get(FOREGROUND_INDEX);
    }

    public BGraphicsA background() {
        return graphics.get(BACKGROUND_INDEX);
    }

    public void draw(Canvas canvas) {
        for (BGraphicsA g : graphics) {
            g.draw(canvas);
        }
    }

    public void resize(int width, int height) {
        Log.d("BG", "width: " + width + " height: " + height);
    }
}
