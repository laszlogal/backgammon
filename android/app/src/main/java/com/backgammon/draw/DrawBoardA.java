package com.backgammon.draw;

import android.graphics.Canvas;

import com.backgammon.board.BoardComponentsA;
import com.backgammon.board.LayeredGraphics;
import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.CheckerSet;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.DrawBoardCommon;

public class DrawBoardA implements DrawBoard {
    private static final double BOARD_RATIO = 0.6;
    private final DrawBoardCommon common;
    private final LayeredGraphics layers;

    public DrawBoardA(BoardComponentsA parts) {
        common = new DrawBoardCommon(parts);
        layers = parts.getLayers();
    }

    public void update(Canvas canvas) {
        layers.draw(canvas);
    }

    @Override
    public void drawBackground() {
        common.drawBackground();
    }

    @Override
    public void drawPoint(Point point) {
        common.drawPoint(point);
    }

    @Override
    public void drawGoal(CheckerSet goal) {
        common.drawGoal(goal);
    }

    @Override
    public void drawBar(Bar bar) {
        common.drawBar(bar);
    }

    @Override
    public void drawDices(int value1, int value2) {
        common.drawDices(value1, value2);
    }

    @Override
    public void resize(int width, int height) {
        int scaledHeight = (int) Math.round(width * BOARD_RATIO);
        layers.resize(width, scaledHeight);
        common.resize(width, scaledHeight);
    }
}