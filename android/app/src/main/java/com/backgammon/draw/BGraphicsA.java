package com.backgammon.draw;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;

import com.backgammon.common.bgraphics.BGraphics;

import androidx.annotation.RequiresApi;

public class BGraphicsA implements BGraphics {

    private final Paint paint;
    private final Bitmap bitmap;
    private Canvas canvas;
    private final Path path;

    public BGraphicsA(Paint paint, Bitmap bitmap) {
        this.paint = paint;
        this.bitmap = bitmap;
        this.canvas = new Canvas(bitmap);
        path = new Path();
    }

    @Override
    public void setFillColor(String color) {
        if (isInvalid()) {
            return;
        }

        paint.setColor(Color.parseColor(color));
    }

    @Override
    public void setLineColor(String color) {

    }

    private boolean isInvalid() {
        return canvas == null;
    }

    @Override
    public void fillRect(int left, int top, int width, int height) {
        if (isInvalid()) {
            return;
        }

        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(left, top, left + width, top + height, paint);
    }

    @Override
    public void setLineWidth(int lineWidth) {
        if (isInvalid()) {
            return;
        }

        paint.setStrokeWidth(lineWidth);
    }

    @Override
    public void strokeRect(int left, int top, int width, int height) {
        if (isInvalid()) {
            return;
        }

        paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(left, top, left + width, top + height, paint);
    }

    @Override
    public void drawLine(int x, int y, int x1, int y1) {
        if (isInvalid()) {
            return;
        }

        canvas.drawLine(x, y, x1, y1, paint);
    }

    @Override
    public void fillCircle(int x, int y, int radius) {
        if (isInvalid()) {
            return;
        }

        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(x, y, radius, paint);
    }

    @Override
    public void beginPath() {
        if (isInvalid()) {
            return;
        }

        path.reset();

    }

    @Override
    public void closePath() {
        if (isInvalid()) {
            return;
        }

        path.close();
        canvas.drawPath(path, paint);
    }

    @Override
    public void moveTo(int x, int y) {
        if (isInvalid()) {
            return;
        }

        path.moveTo(x, y);
    }

    @Override
    public void lineTo(int x, int y) {
        if (isInvalid()) {
            return;
        }

        path.lineTo(x, y);
    }

    @Override
    public void save() {

    }

    @Override
    public void restore() {

    }

    @Override
    public void clear(int x, int y, int width, int height) {

    }

    public void setFillStyle(android.graphics.Paint.Style style) {
        if (isInvalid()) {
            return;
        }

        paint.setStyle(style);
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, 0, 0, paint);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void resize(int width, int height) {
        bitmap.setWidth(width);
        bitmap.setHeight(height);
    }
}
