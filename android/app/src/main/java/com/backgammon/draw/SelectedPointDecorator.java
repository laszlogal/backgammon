package com.backgammon.draw;

import android.graphics.Paint;

import com.backgammon.common.game.draw.triangles.BGraphicsDecorator;

public class SelectedPointDecorator implements BGraphicsDecorator {
    private final BGraphicsA g;

    public SelectedPointDecorator(BGraphicsA graphics) {
        this.g = graphics;
    }

    @Override
    public void setColor() {

    }

    @Override
    public void decorate() {
        g.setLineWidth(28);
        g.setFillStyle(Paint.Style.FILL);
        g.setFillColor("yellow");
    }
}
