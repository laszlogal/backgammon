package com.backgammon.draw;

import android.graphics.Paint;

import com.backgammon.common.game.draw.triangles.BGraphicsDecorator;

public class OddPointDecorator implements BGraphicsDecorator {
    private final BGraphicsA g;

    public OddPointDecorator(BGraphicsA graphics) {
        this.g = graphics;
    }

    @Override
    public void setColor() {

    }

    public void decorate() {
        g.setFillStyle(Paint.Style.FILL);
        g.setFillColor("#ff0000");
    }
}
