package com.backgammon.draw;

import android.graphics.Paint;

import com.backgammon.common.game.draw.triangles.BGraphicsDecorator;

public class EvenPointDecorator implements BGraphicsDecorator {
    private final BGraphicsA g;

    public EvenPointDecorator(BGraphicsA graphics) {
        this.g = graphics;
    }

    @Override
    public void setColor() {

    }

    @Override
    public void decorate() {
        g.setFillStyle(Paint.Style.STROKE);
        g.setFillColor("#000000");
        g.setLineWidth(14);
    }
}
