package com.backgammon;

import com.backgammon.common.game.InfoCenter;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class InfoCenterA implements InfoCenter {
    private Context context;

    public InfoCenterA(Context context) {
        this.context = context;
    }

    @Override
    public void win(String name) {
        showLong(name + " WINS!");
    }

    @Override
    public void setText(String text) {
        showLong(text);
    }

    private void showLong(String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }
}
