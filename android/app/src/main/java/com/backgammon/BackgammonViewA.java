package com.backgammon;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.backgammon.board.BoardViewA;
import com.backgammon.common.game.board.BoardView;

public class BackgammonViewA extends View {
    private BoardViewA view;

    public BackgammonViewA(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        view.update(canvas);
        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view.resize(w, h);
    }

    public void setBoardView(BoardView view) {
        this.view = (BoardViewA) view;
    }

}
