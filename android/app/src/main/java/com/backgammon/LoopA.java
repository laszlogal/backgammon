package com.backgammon;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;

import com.backgammon.common.bgraphics.ILoop;
import com.backgammon.common.game.draw.AnimListener;

public class LoopA implements ILoop, AnimatorUpdateListener, AnimatorListener {
    private final AnimListener listener;
    private final View view;
    private final ValueAnimator animator;

    public LoopA(AnimListener listener, View view) {
        this.listener = listener;
        this.view = view;
        animator = ValueAnimator.ofInt(1, 500);
        animator.setDuration(2000);
        animator.addUpdateListener(this);
        animator.addListener(this);
    }


    @Override
    public void start() {
        animator.start();
    }

    @Override
    public void stop() {
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        listener.onEnd();
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        view.invalidate();
        listener.run();
    }
}
