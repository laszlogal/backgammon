package com.backgammon.util;

import com.backgammon.common.util.BGLogger;

import android.util.Log;

public class LoggerA implements BGLogger {

    public static final String TAG="BACKGAMMON";

    @Override
    public void debug(String message) {
        Log.d(TAG, message);
    }

    @Override
    public void info(String message) {
        debug(message);
    }
}
