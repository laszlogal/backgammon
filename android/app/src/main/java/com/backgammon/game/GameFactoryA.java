package com.backgammon.game;

import com.backgammon.BackgammonViewA;
import com.backgammon.InfoCenterA;
import com.backgammon.LoopA;
import com.backgammon.ShuffleDiceAnimationA;
import com.backgammon.board.BoardComponentsA;
import com.backgammon.board.BoardViewA;
import com.backgammon.common.game.GameFactory;
import com.backgammon.common.game.InfoCenter;
import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.point.BoardController;
import com.backgammon.common.game.board.point.BoardControllerImpl;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.ShuffleDice;
import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.agent.HumanAgent;
import com.backgammon.common.game.player.agent.RandomMovesAgent;
import com.backgammon.common.util.BGLogger;
import com.backgammon.draw.DrawBoardA;
import com.backgammon.util.LoggerA;

public class GameFactoryA implements GameFactory {

    private BoardComponentsA components;
    private DrawBoardA drawBoardA;
    private ShuffleDiceAnimationA diceAnimation;
    private BackgammonViewA view;

    public GameFactoryA(BackgammonViewA view, int width, int height) {
        this.view = view;
        components = new BoardComponentsA(width, height);
        view.setOnTouchListener(components.getBoardOnTouchListener());
    }

    @Override
    public void createGui(BoardView view) {

    }

    @Override
    public BGLogger createLogger() {
         return new LoggerA();
    }

    @Override
    public DrawBoard createDrawBoard() {
        drawBoardA = new DrawBoardA(components);
        return drawBoardA;
    }

    @Override
    public BoardView createView(Board board, DrawBoard drawBoard) {
        BoardViewA boardView = new BoardViewA(board, drawBoardA, components.getStepLoop());
        view.setBoardView(boardView);
        return boardView;
    }

    @Override
    public void createDiceAnimation(ShuffleDice shuffleDice) {
        diceAnimation = new ShuffleDiceAnimationA(shuffleDice);
        LoopA loop = new LoopA(diceAnimation, view);
        diceAnimation.setLoop(loop);

    }

    @Override
    public BoardController createBoardController() {
        return new BoardControllerImpl(components.getDrawPointTriangles());
    }

    @Override
    public void onGameStart() {

    }

    @Override
    public Runnable diceStart() {
        return diceAnimation::start;
    }

    @Override
    public BackgammonAgent playerAgent1() {
        return newHumanAlgorithm();
    }

    @Override
    public BackgammonAgent playerAgent2() {
        return new RandomMovesAgent();
    }

    @Override
    public InfoCenter infoCenter() {
        return new InfoCenterA(view.getContext());
    }

    public BackgammonAgent newHumanAlgorithm() {
        return new HumanAgent(createBoardController(),
                components.getBoardOnTouchListener());
    }

    public DrawBoardA getDrawBoard() {
        return drawBoardA;
    }
}
