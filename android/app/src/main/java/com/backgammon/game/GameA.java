package com.backgammon.game;

import android.graphics.Canvas;

import com.backgammon.common.game.Game;
import com.backgammon.draw.DrawBoardA;

public class GameA extends Game {
    private final DrawBoardA drawBoard;
    private final GameFactoryA factory;

    public GameA(GameFactoryA factory) {
        super(factory);
        drawBoard = factory.getDrawBoard();
        this.factory = factory;
    }

    public void dices() {
        factory.diceStart().run();
    }

    public void update(Canvas canvas) {
        drawBoard.update(canvas);
        view.update();
    }

    public void resize(int width, int height) {
        view.resize(width, height);
    }
}
