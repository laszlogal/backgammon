package com.backgammon.common.console;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.CheckerSet;
import com.backgammon.common.game.draw.DrawBoard;

public class DrawConsoleBoard implements DrawBoard {

	@Override
	public void drawBackground() {
		// implement me
	}

	@Override
	public void drawPoint(Point point) {
		// implement me
	}

	@Override
	public void drawGoal(CheckerSet goal) {
		// implement me
	}

	@Override
	public void resize(int width, int height) {
		// no resize for console
	}

	@Override
	public void drawBar(Bar bar) {
		// implement me
	}

	@Override
	public void drawDices(int value1, int value2) {
		// implement me
	}
}
