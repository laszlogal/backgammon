package com.backgammon.common.console;

import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.Roll;
import com.backgammon.common.util.Log;

public class BoardViewConsole implements BoardView {

	@Override
	public void update() {
		// implement me
	}

	@Override
	public void resize(int width, int height) {
		// not applicable
	}

	@Override
	public void animateMove(MoveChecker move, MoveFinishedHandler handler) {
		handler.onMoveFinished(move);
	}

	@Override
	public void setRoll(Roll roll) {
		Log.debug("The roll is: " + roll);
	}

	@Override
	public void updateDices(Roll roll) {
		// implement me
	}
}
