package com.backgammon.common.console;

import com.backgammon.common.game.Game;

public class Backgammon {
	public static void main(String[] args) {
		Game game = new Game(new ConsoleGameFactory());
		game.start();
	}
}
