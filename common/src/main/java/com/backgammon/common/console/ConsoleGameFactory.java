package com.backgammon.common.console;

import com.backgammon.common.game.GameFactory;
import com.backgammon.common.game.InfoCenter;
import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.point.BoardController;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.ShuffleDice;
import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.agent.RandomMovesAgent;
import com.backgammon.common.util.JBGLogger;
import com.backgammon.common.util.Log;
import com.backgammon.common.util.BGLogger;

public class ConsoleGameFactory implements GameFactory {
	private ShuffleDice shuffleDice;

	@Override
	public void createGui(BoardView view) {
		// no gui
	}

	@Override
	public BGLogger createLogger() {
		return new JBGLogger();
	}

	@Override
	public DrawBoard createDrawBoard() {
		return new DrawConsoleBoard();
	}

	@Override
	public BoardView createView(Board board, DrawBoard drawBoard) {
		return new BoardViewConsole();
	}

	@Override
	public void createDiceAnimation(ShuffleDice shuffleDice) {
		this.shuffleDice = shuffleDice;
	}

	@Override
	public BoardController createBoardController() {
		return null;
	}

	@Override
	public void onGameStart() {
		Log.debug("Game has started");
	}

	@Override
	public Runnable diceStart() {
		return () -> {
			shuffleDice.next();
			shuffleDice.processResult();
		};
	}

	@Override
	public BackgammonAgent playerAgent1() {
		return new RandomMovesAgent();
	}

	@Override
	public BackgammonAgent playerAgent2() {
		return new RandomMovesAgent();
	}

	@Override
	public InfoCenter infoCenter() {
		return null;
	}
}
