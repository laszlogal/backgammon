package com.backgammon.common.bgraphics;

import com.backgammon.common.game.draw.AnimListener;

public abstract class Loop implements ILoop {
	private final int speed;
	private final int maxFrames;
	private final AnimListener listener;
	private final double fps;
	int frame;
	private boolean running = false;
	private double fpsInterval;
	private double past;
	private int speedTimer = 0;

	protected Loop(AnimListener listener, int speed, int maxFrames) {
		this.listener = listener;
		this.speed = speed;
		this.maxFrames = maxFrames;
		fps = 60;
	}

	@Override
	public void start() {
		fpsInterval = 1000.0 / fps;
		past = timeNow();
		frame = 0;
		speedTimer = 0;
		running = true;
		scheduleAnimation();
	}

	public long timeNow() {
		return System.currentTimeMillis();
	}

	public abstract void scheduleAnimation();

	public void execute() {
		if (!running) {
			return;
		}

		double now = timeNow();
		double elapsed = now - past;
		if (fpsInterval < elapsed) {
			past = now - (fpsInterval % elapsed);
			if (speedTimer == speed) {
				animate();
			}
			speedTimer++;
		}

		requestNewFrame();

	}

	protected void requestNewFrame() {
		if (hasMaxFrame()) {
			scheduleUntilMaxFrame();
		} else {
			scheduleAnimation();
		}
	}

	public void animate() {
		listener.run();
		frame++;
		speedTimer = 0;
	}

	private void scheduleUntilMaxFrame() {
		if (frame < maxFrames) {
			scheduleAnimation();
		} else {
			listener.onEnd();
		}
	}

	private boolean hasMaxFrame() {
		return maxFrames > 0;
	}

	@Override
	public void stop() {
		running = false;
		listener.onEnd();
	}
}
