package com.backgammon.common.bgraphics;

public interface ILoop {
    void start();

    void stop();
}
