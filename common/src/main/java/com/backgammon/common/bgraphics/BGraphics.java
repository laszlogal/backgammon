package com.backgammon.common.bgraphics;

public interface BGraphics {
	void setFillColor(String color);

	void setLineColor(String color);

	void fillRect(int left, int top, int width, int height);

	void setLineWidth(int lineWidth);

	void strokeRect(int left, int top, int width, int height);

	void drawLine(int x, int y, int x1, int y1);

	void fillCircle(int x, int y, int radius);

	void beginPath();

	void closePath();

	void moveTo(int x, int y);

	void lineTo(int x, int y);

	void save();

	void restore();

	void clear(int x, int y, int width, int height);
}
