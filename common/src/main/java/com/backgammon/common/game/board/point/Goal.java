package com.backgammon.common.game.board.point;

import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;

public class Goal implements MutableCheckerSet {

	private final int goalIndex;
	private final Point point;

	public Goal(int goalIndex, Point point) {
		this.goalIndex = goalIndex;
		this.point = point;
	}

	@Override
	public Checker checker() {
		return point.checker();
	}

	@Override
	public int count() {
		return point.count();
	}

	@Override
	public int index() {
		return goalIndex;
	}

	@Override
	public void set(Checker checker, int count) {
		point.set(checker, count);
	}

	@Override
	public String toString() {
		return "GOAL " + point;
	}

	@Override
	public boolean isGoal() {
		return true;
	}

	@Override
	public boolean isBar() {
		return false;
	}
}
