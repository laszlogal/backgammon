package com.backgammon.common.game.board;

import com.backgammon.common.game.draw.BoardMetrics;
import com.backgammon.common.game.draw.DrawBackground;
import com.backgammon.common.game.draw.DrawCheckers;
import com.backgammon.common.game.draw.DrawDices;
import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.draw.triangles.DrawPointTriangles;

public interface BoardComponents {
	BoardMetrics getMetrics();

	DrawBackground getBackground();

	DrawPointTriangles getDrawPointTriangles();

	DrawCheckers getCheckers();

	DrawDices getDices();

	StepLoop getStepLoop();
}
