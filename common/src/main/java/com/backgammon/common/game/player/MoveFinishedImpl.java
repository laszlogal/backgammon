package com.backgammon.common.game.player;

import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.util.Log;

public class MoveFinishedImpl implements MoveFinishedHandler {
	private final Turn turn;
	private final BoardView view;

	public MoveFinishedImpl(Turn turn, BoardView view) {
		this.turn = turn;
		this.view = view;
	}

	@Override
	public void onMoveFinished(MoveChecker move) {
		executeMove(move);
		turn.nextMove(move);
	}

	private void executeMove(MoveChecker move) {
		logMove(move);
		move.execute();
		view.update();
	}

	private void logMove(MoveChecker move) {
		Log.debug(move.toString());
	}
}
