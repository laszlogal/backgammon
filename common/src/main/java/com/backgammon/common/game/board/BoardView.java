package com.backgammon.common.game.board;

import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.Roll;

public interface BoardView {
	void update();

	void resize(int width, int height);

	void animateMove(MoveChecker move, MoveFinishedHandler handler);

	void setRoll(Roll roll);

	void updateDices(Roll roll);
}
