package com.backgammon.common.game.draw.triangles;

public class PointDecorators {
    private final BGraphicsDecorator odd;
    private final BGraphicsDecorator even;
    private final BGraphicsDecorator selected;

    public PointDecorators(BGraphicsDecorator odd, BGraphicsDecorator even, BGraphicsDecorator selected) {
        this.odd = odd;
        this.even = even;
        this.selected = selected;
    }

    public BGraphicsDecorator selected() {
        return selected;
    }

    public BGraphicsDecorator get(int number) {
        return number % 2 == 0 ? even : odd;
    }
}
