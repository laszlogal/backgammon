package com.backgammon.common.game.draw;

import com.backgammon.common.bgraphics.BGraphics;
import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.point.checker.CheckerSet;

public class DrawCheckers {
	public static final int ERASER = 2;
	private final DrawChecker drawChecker;
	private final BoardMetrics metrics;
	private final BGraphics g;
	private final DrawPoint drawPoint;

	public DrawCheckers(BoardMetrics metrics, DrawChecker drawChecker, BGraphics graphics) {
		this.metrics = metrics;
		this.g = graphics;
		this.drawChecker = drawChecker;
		drawPoint = new DrawPoint(metrics, drawChecker, g);
	}

	public void drawPoint(int number, Point point) {
		drawPoint.draw(number, point);
	}

	CheckerPosition newCheckerPosition(int number, int amount) {
		return drawPoint.newCheckerPosition(number, amount);
	}

	public void clearLastChecker(int number, int amount) {
		if (amount == 0) {
			return;
		}

		CheckerPosition position = drawPoint.newCheckerPosition(number, amount);
		clearCheckerAt(amount, position);
	}

	private void clearCheckerAt(int amount, CheckerPosition position) {
		int x = position.x();
		int y = position.y() + (amount - 1) * position.gap();
		drawChecker.clear(x, y, g, ERASER);
	}

	public void drawGoal(CheckerSet goal) {
		if (goal.count() == 0) {
			return;
		}
		drawPoint.drawCheckers(getGoalPosition(goal), goal.count(), goal.checker());
	}

	public CheckerPosition getGoalPosition(CheckerSet goal) {
		int x = metrics.getRight() - metrics.getGoalWidth() + (metrics.getGoalWidth() - 2 * metrics.getCheckerRadius() - 6) / 2;
		int y;
		int diff =  drawPoint.getCheckerDiff(goal.count());
		if (goal.checker() == Checker.WHITE) {
			y = metrics.getUpperPointY();
		} else {
			diff *= -1;
			y = metrics.getBottomPointY();
		}
		return new CheckerPosition(x, y, diff);
	}

	public void drawBar(Bar bar) {
		int whiteCount = bar.jail(Checker.WHITE).count();
		int blackCount = bar.hits(Checker.WHITE).count();
		drawPoint.drawCheckers(getWhiteHitPosition(whiteCount), whiteCount, Checker.WHITE);
		drawPoint.drawCheckers(getBlackHitPosition(blackCount), blackCount, Checker.BLACK);
	}

	public CheckerPosition getWhiteHitPosition(int count) {
		return new CheckerPosition(hitX(), hitY(count),
				-drawPoint.getCheckerDiff(count));
	}

	public CheckerPosition getBlackHitPosition(int count) {
		int y = hitY(count) + 2 * metrics.getCheckerRadius();
		return new CheckerPosition(hitX(), y, drawPoint.getCheckerDiff(count));
	}

	public int hitY(int count) {
		return metrics.getMiddleY() - (drawPoint.getCheckerDiff(count)) / 2;
	}

	public int hitX() {
		return metrics.getMiddleX() - metrics.getPointWidth() / 2;
	}

	public void update(BoardMetrics metrics) {
		drawPoint.update(metrics);
	}

	public void clearLastCheckerFromBar(Checker checker, int count) {
		CheckerPosition position = checker == Checker.WHITE ? getWhiteHitPosition(count)
				: getBlackHitPosition(count);
		clearCheckerAt(count, position);
	}
}
