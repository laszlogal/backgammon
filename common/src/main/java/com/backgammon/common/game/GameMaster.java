package com.backgammon.common.game;

public interface GameMaster {

	void newGame();

	void onRollEnd(int diceValue1, int diceValue2);

	void endTurn();

	void says(String message);

	void update();
}
