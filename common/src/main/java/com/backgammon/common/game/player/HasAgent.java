package com.backgammon.common.game.player;

import com.backgammon.common.game.player.agent.HumanAgent;

public interface HasAgent {
	void setAgent(HumanAgent agent);
}
