package com.backgammon.common.game.player.agent;

import com.backgammon.common.game.GameMaster;
import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.BoardController;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.PointInterface;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;
import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.BoardAccess;
import com.backgammon.common.game.player.HasAgent;
import com.backgammon.common.game.player.Turn;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.MovesAvailable;
import com.backgammon.common.util.Log;

public class HumanAgent implements BackgammonAgent {
	private final BoardController controller;
	private Turn turn;
	private PointInterface source;
	private MovesAvailable available;
	private PlayerRelativeBoard playerBoard=null;
	private ProcessStatus status = ProcessStatus.SELECT_SOURCE;
	private BoardAccess access=null;
	private GameMaster gameMaster;

    private enum ProcessStatus {
		SELECT_SOURCE,
		SELECT_DESTINATION
	}

	public HumanAgent(BoardController controller, HasAgent eventHandler) {
		this.controller = controller;
		eventHandler.setAgent(this);
	}

	@Override
	public void chooseFrom(Turn turn, MovesAvailable available) {
		this.available = available;
		playerBoard = available.getPlayerBoard();
		access = playerBoard.access();
		this.turn = turn;
		gameMaster = turn.getGameMaster();
		gameMaster.says(this.access.checker() + ", you are next!");
	}

	public boolean isInTurn() {
		return turn != null && turn.isInTurn();
	}

	public void process(int x, int y) {
		if (!isInTurn()) {
			gameMaster.says("It's not your turn!");
			return;
		}

		if (playerBoard == null) {
			return;
		}
		int absoluteNumber = pointNumberAt(x, y);
		if (absoluteNumber < 0 || absoluteNumber > 24) {
			return;
		}

		int number = access.toIndex(absoluteNumber) + 1;
		if (isSourceSelect()) {
			if (playerBoard.jail().count() > 0) {
				selectJailAsSource();
			} else {
				selectSource(number);
			}
		} else {
			selectDestination(number);
		}
		gameMaster.update();
	}

	private boolean isSourceSelect() {
		return status == ProcessStatus.SELECT_SOURCE;
	}

	private void selectJailAsSource() {
		source = playerBoard.jail();
		onSourceSelect();
	}

	private int pointNumberAt(int x, int y) {
		return controller.getHitPoint(x, y);
	}

	private void selectSource(int number) {
		source = playerBoard.point(number);
		if (source.checker() != access.checker()) {
			gameMaster.says("That's not your point!");
			return;
		}
		onSourceSelect();
	}

	private void clearSource() {
		controller.clearSelection();
		source = null;
		status = ProcessStatus.SELECT_SOURCE;
	}

	private void onSourceSelect() {
		showAvailableMoves();
		status = ProcessStatus.SELECT_DESTINATION;
	}

	private void log(String message) {
		Log.debug("[HUMAN] " + message);
	}

	private void showAvailableMoves() {
    	controller.highlight(source.index() + 1);
		for (MoveChecker move: available) {
			if (move.getSource() == source) {
				highlightMove(move);
			}
			log("Available: " + move);
		}
	}

	private void highlightMove(MoveChecker move) {
		MutableCheckerSet destination = move.getDestination();
		controller.highlight(destination.index() + 1);
	}

	private void clearAvailableMoves() {
		for (MoveChecker move: available) {
			if (move.getSource() == source) {
				clearMoveHighlight(move);
			}
		}
	}


	private void clearMoveHighlight(MoveChecker move) {
		MutableCheckerSet destination = move.getDestination();
		controller.clearHighlight(destination.index() + 1);
	}

	private void selectDestination(int number) {
		Point destination = playerBoard.point(number);
		if (destination == source) {
			log("source cleared.");
			clearSource();
			return;
		}

		log("destination selected: " + number);
		MoveChecker move = new MoveChecker(source, destination);
		if (available.contains(move)) {
			log("move is " + move);
			controller.clearSelection();
			turn.move(move);
		} else {
			log("Wrong move: " + move);
		}
		status = ProcessStatus.SELECT_SOURCE;
	}
}
