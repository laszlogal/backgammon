package com.backgammon.common.game.factory;

import static com.backgammon.common.game.factory.Points.blackBar;
import static com.backgammon.common.game.factory.Points.whiteBar;

import java.util.Arrays;
import java.util.List;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardImpl;
import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.BarImpl;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.Checker;

public class BoardFactory {

	public static final int WHITE_GOAL_INDEX = 0;
	public static final int BLACK_GOAL_INDEX = 25;
	private List<Point> points;
	private int whiteGoalCount = 0;
	private int blackGoalCount = 0;
	private int whiteHits = 0;
	private int blackHits = 0;

	public BoardFactory withPoints(List<Point> points) {
		this.points = points;
		return this;
	}

	public BoardFactory withGoals(int whiteGoalCount, int blackGoalCount) {
		this.whiteGoalCount = whiteGoalCount;
		this.blackGoalCount = blackGoalCount;
		return this;
	}

	public BoardFactory withHits(int whiteHits, int blackHits) {
		this.whiteHits = whiteHits;
		this.blackHits = blackHits;
		return this;
	}

	public Board build() {
		if (points == null) {
			points = PointFactory.empty();
		}
		return new BoardImpl(points, createGoals(), createBar());
	}

	private Bar createBar() {
		return new BarImpl(whiteBar(whiteHits), blackBar(blackHits));
	}

	public List<Goal> createGoals() {
		return Arrays.asList(createGoal(WHITE_GOAL_INDEX, Checker.WHITE, whiteGoalCount),
				createGoal(BLACK_GOAL_INDEX, Checker.BLACK, blackGoalCount));
	}

	public Goal createGoal(int goalIndex, Checker checker, int count) {
		return new Goal(goalIndex, new Point(checker, count));
	}
}
