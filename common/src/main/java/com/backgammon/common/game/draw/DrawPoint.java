package com.backgammon.common.game.draw;

import com.backgammon.common.bgraphics.BGraphics;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.point.checker.CheckerSet;

public class DrawPoint {
	private static final int GAP = 2;
	private final BoardMetrics metrics;
	private final DrawChecker drawChecker;
	private final BGraphics g;

	public DrawPoint(BoardMetrics metrics, DrawChecker drawChecker, BGraphics g) {
		this.metrics = metrics;
		this.drawChecker = drawChecker;
		this.g = g;
	}

	public void draw(int number, CheckerSet point) {
		if (point.count() == 0) {
			return;
		}

		CheckerPosition position = newCheckerPosition(number, point.count());
		drawCheckers(position, point.count(), point.checker());
	}

	CheckerPosition newCheckerPosition(int number, int amount) {
		if (isUpperPart(number)) {
			return new CheckerPosition(metrics.getUpperPointX(number),
					metrics.getUpperPointY(),
					getCheckerDiff(amount));
		} else {
			return new CheckerPosition(metrics.getBottomPointX(number),
					metrics.getBottomPointY(), -getCheckerDiff(amount));
		}

	}

	private boolean isUpperPart(int number) {
		return number < 13;
	}

	int getCheckerDiff(int amount) {
		return amount == 0 ? 0
				: Math.min(2 * metrics.getCheckerRadius() + GAP, triangleHeight() / amount);
	}

	private int triangleHeight() {
		return (int) (5.5 * (metrics.getCheckerRadius() * 2));
	}

	void drawCheckers(CheckerPosition position, int amount, Checker checker) {
		int x = position.x();
		int y = position.y();
		for (int i = 0; i < amount; i++) {
			drawChecker.draw(x, y, checker, g);
			y += position.gap();
		}
	}

	public void update(BoardMetrics metrics) {
		drawChecker.update(metrics.triangleMiddle(), metrics.getCheckerRadius());
	}

}