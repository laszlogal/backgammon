package com.backgammon.common.game.player.move;

import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;
import com.backgammon.common.util.Log;

public class MoveValidator {
	private final PlayerRelativeBoard playerBoard;

	public MoveValidator(PlayerRelativeBoard playerBoard) {
		this.playerBoard = playerBoard;
	}

	public boolean isValid(int sourceIndex, int destinationIndex) {
		return isValid(playerBoard.point(sourceIndex), playerBoard.point(destinationIndex));
	}

	public boolean isValid(MutableCheckerSet source, MutableCheckerSet destination) {
		return source != destination
				&& (isValidInGameMove(source, destination) || isValidBearOffMove(destination));
	}

	private boolean isValidBearOffMove(MutableCheckerSet destination) {
		return isGoal(destination) && isBearOff();
	}

	private boolean isValidInGameMove(MutableCheckerSet source, MutableCheckerSet destination) {
		if (isGoal(destination) || !isDecreasingOrder(source, destination)) {
			return false;
		}

		return isNormalMove(source, destination) || isBlot(source, destination);
	}

	private boolean isNormalMove(MutableCheckerSet source, MutableCheckerSet destination) {
		return isEmpty(destination) || (hasSameOwner(source, destination));
	}


	private boolean isDecreasingOrder(MutableCheckerSet source, MutableCheckerSet destination) {
		return destination.index() != source.index();
	}

	private boolean isGoal(MutableCheckerSet destination) {
		return destination == playerBoard.goal();
	}

	public boolean isBlot(MutableCheckerSet source, MutableCheckerSet destination) {
		return destination.count() == 1 && !hasSameOwner(source, destination);
	}

	private boolean hasSameOwner(MutableCheckerSet source, MutableCheckerSet destination) {
		return destination.checker() == source.checker();
	}

	private boolean isEmpty(MutableCheckerSet destination) {
		return destination.count() == 0;
	}

	public boolean isBearOff() {
		int count = playerBoard.goalCount();
		for (int number = 6; number > 0; number--) {
			if (playerBoard.owns(number)) {
				count += playerBoard.point(number).count();
			}
		}
		boolean bearOff = count == 15;
		if (bearOff) {
			Log.debug("BEAR OFF");
		}
		return bearOff;
	}
}
