package com.backgammon.common.game;

import java.util.Arrays;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.ShuffleDice;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.factory.PlayerBuilder;
import com.backgammon.common.game.player.Player;
import com.backgammon.common.util.Log;

public class Game {
	private ShuffleDice shuffleDice;
	private final GameFactory factory;
	protected DrawBoard drawBoard;
	private Board board;
	private GameMaster gameMaster;
	protected BoardView view;

	public Game(GameFactory factory) {
		this.factory = factory;
		setLogger();
		createGameComponents();
	}

	private void setLogger() {
		Log.setLogger(factory.createLogger());
	}

	private void createGameComponents() {
		createDrawBoard();
		createDices();
		createGameMaster();
		createGui();
	}

	private void createDrawBoard() {
		drawBoard = factory.createDrawBoard();
	}


	private void createDices() {
		shuffleDice = new ShuffleDice(drawBoard);
		factory.createDiceAnimation(shuffleDice);
	}

	private void createGameMaster() {
		board = createBoard();
		view = factory.createView(board, drawBoard);
		gameMaster = new GameMasterImp(shuffleDice, view,
				Arrays.asList(createPlayer1(),
						createPlayer2()),
				factory.infoCenter(),
				factory.diceStart());
	}

	private Board createBoard() {
		BoardFactory boardFactory = new BoardFactory();
		return boardFactory.build();
	}

	private Player createPlayer1() {
		PlayerBuilder builder = getBuilder();
		builder.setForward();
		builder.setAgent(factory.playerAgent1());
		return builder.getResult();
	}

	private PlayerBuilder getBuilder() {
		PlayerBuilder builder = new PlayerBuilder(board, view);
		builder.setStandardSetup();
		return builder;
	}

	private Player createPlayer2() {
		PlayerBuilder builder = getBuilder();
		builder.setBackward();
		builder.setAgent(factory.playerAgent2());
		return builder.getResult();
	}

	private void createGui() {
		factory.createGui(view);
	}

	public void start() {
		factory.onGameStart();
		gameMaster.newGame();
	}
}
