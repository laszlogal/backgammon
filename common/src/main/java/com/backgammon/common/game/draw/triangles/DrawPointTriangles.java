package com.backgammon.common.game.draw.triangles;

import java.util.ArrayList;
import java.util.List;

import com.backgammon.common.bgraphics.BGraphics;
import com.backgammon.common.game.draw.BoardMetrics;

public class DrawPointTriangles {
	public static final int VERTICAL_MARGIN = 12;
	public static final int FIRST_UPPER_POINT = 1;
	public static final int LAST_UPPER_POINT = 12;
	public static final int FIRST_LOWER_POINT = 13;
	public static final int LAST_LOWER_POINT = 24;
	private final BoardMetrics metrics;
	private final DrawDownTriangle drawDownTriangle;
	private final DrawUpTriangle drawUpTriangle;
	private final PointDecorators decorators;
	private final List<Triangle> triangles;
	private final List<Integer> selection;
	public DrawPointTriangles(BoardMetrics metrics, BGraphics graphics,
							  PointDecorators decorators) {
		this.metrics = metrics;
		drawDownTriangle = new DrawDownTriangle(graphics);
		drawUpTriangle = new DrawUpTriangle(graphics);
		this.decorators = decorators;
		this.triangles = new ArrayList<>();
		this.selection = new ArrayList<>();
		createTriangles();
	}

	private void createTriangles() {
		triangles.clear();
		forEachUpperPoint(number -> triangles.add(createDownTriangle(number)));
		forEachLowerPoint(number -> triangles.add(createUpTriangle(number)));
	}

	public void resize() {
		createTriangles();
	}

	private void forEachUpperPoint(NumberRunnable consumer) {
		forEachPoint(FIRST_UPPER_POINT, LAST_UPPER_POINT, consumer);
	}

	private void forEachPoint(int start, int end, NumberRunnable consumer) {
		for (int number = start; number < end + 1; number++) {
			consumer.run(number);
		}
	}

	private void forEachLowerPoint(NumberRunnable consumer) {
		forEachPoint(FIRST_LOWER_POINT, LAST_LOWER_POINT + 1, consumer);
	}

	private Triangle createUpTriangle(int number) {
		int top = metrics.getBottom() - triangleHeight() - BoardMetrics.CHECKER_MARGIN;
		int x = metrics.getBottomPointX(number);
		return new Triangle(number, x, top,
				metrics.getPointWidth(), triangleHeight(),
				drawUpTriangle);
	}

	private int triangleHeight() {
		return (int) (5.5 * (metrics.getCheckerRadius() * 2));
	}

	private Triangle createDownTriangle(int number) {
		int top = metrics.getTop() + VERTICAL_MARGIN;
		int x = metrics.getUpperPointX(number);
		return new Triangle(number, x,
				top, metrics.getPointWidth(), triangleHeight(), drawDownTriangle);
	}

	public void draw(int number) {
		draw(number, decorators.get(number));
	}

	public void drawSelection() {
		for (Integer number: selection) {
			draw(number, decorators.selected());
		}
	}

	public void draw(int number, BGraphicsDecorator decorator) {
		triangles.get(number - 1).draw(decorator);
	}

	public int pointAt(int x, int y) {
		for (Triangle triangle : triangles) {
			if (triangle.isHit(x, y)) {
				return triangle.number;
			}

		}
		return -1;
	}

	public void select(int number) {
		selection.add(number);
	}

	public void deselect(int number) {
		selection.remove(number);
	}

	public void clearSelection() {
		selection.clear();
	}
}
