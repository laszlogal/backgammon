package com.backgammon.common.game.draw;

import com.backgammon.common.bgraphics.BGraphics;
import com.backgammon.common.game.board.point.checker.Checker;

public class DrawChecker {
	private int oX;
	private int radius;
	private int borderWidth;

	public DrawChecker(int oX, int radius) {
		update(oX, radius);
	}

	void draw(int x, int y, Checker checker, BGraphics g) {
		g.setFillColor(checker.getBorderColor());
		g.fillCircle(x + oX, y, radius);
		g.setFillColor(checker.getColor());
		g.fillCircle(x + oX, y, radius - borderWidth);
	}

	public void clear(int x, int y, BGraphics g, int eraser) {
		int diameter = 2 * radius + eraser;
		g.clear(x - eraser, y - radius - eraser, diameter, diameter);

	}

	public void update(int oX, int radius) {
		this.oX = oX;
		this.radius = radius;
		borderWidth = Math.max(this.radius / 5, 1);
	}
}