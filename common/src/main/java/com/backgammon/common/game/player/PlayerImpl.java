package com.backgammon.common.game.player;

import com.backgammon.common.game.GameMaster;
import com.backgammon.common.game.player.move.Roll;

public class PlayerImpl implements Player {
	private final Turn turn;
	private final String name;

	public PlayerImpl(Turn turn, String name, BackgammonAgent agent) {
		this.turn = turn;
		this.name = name;
		turn.setAgent(agent);
	}

	@Override
	public void startTurn(Roll roll) {
		turn.start(roll);
	}

	@Override
	public boolean isWinner() {
		return turn.isWinner();
	}

	public void setGameMaster(GameMaster gameMaster) {
		turn.setGameMaster(gameMaster);
	}

	@Override
	public String toString() {
		return name;
	}

}
