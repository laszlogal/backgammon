package com.backgammon.common.game.board;

import java.util.Collection;
import java.util.EnumMap;
import java.util.List;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.BarPoint;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.setup.CheckerSetup;
import com.backgammon.common.game.player.BoardAccess;

public class PlayerRelativeBoardImpl implements PlayerRelativeBoard {
	private final Bar bar;
	private final List<Point> points;
	private final EnumMap<Checker, Goal> goals;
	private final BoardAccess access;
	private final Checker checker;

	public PlayerRelativeBoardImpl(Board board, BoardAccess access) {
		points = board.points();
		bar = board.bar();
		this.access = access;
		this.checker = access.checker();
		this.goals = new EnumMap<>(Checker.class);
		createGoalMap(board.goals());
	}

	public PlayerRelativeBoardImpl(Board board, BoardAccess access, CheckerSetup checkerSetup) {
		this(board, access);
		setup(checkerSetup);
	}

	private void createGoalMap(Collection<Goal> goals) {
		goals.forEach((goal -> this.goals.put(goal.checker(), goal)));
	}

	@Override
	public Goal goal() {
		return goals.get(checker);
	}

	@Override
	public BarPoint hits() {
		return bar.hits(checker);
	}

	@Override
	public BarPoint jail() {
		return bar.jail(checker);
	}

	@Override
	public Point point(int number) {
		return points.get(access.toIndex(number));
	}

	@Override
	public boolean owns(int number) {
		return point(number).checker() == checker;
	}

	@Override
	public int goalCount() {
		return goal().count();
	}

	@Override
	public void set(int number, int count) {
		point(number).set(checker, count);
	}

	@Override
	public void setOpposite(int number, int count) {
		point(number).set(checker.opposite(), count);
	}

	@Override
	public void setup(CheckerSetup setup) {
		setup.run(this);
	}

	@Override
	public boolean isGoalFilled() {
		return goalCount() == 15;
	}

	@Override
	public BoardAccess access() {
		return access;
	}

	@Override
	public void makeAHit() {
		hits().set(access.checker(), hits().count() + 1);
	}
}
