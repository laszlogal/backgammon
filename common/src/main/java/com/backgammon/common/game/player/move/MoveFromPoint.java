package com.backgammon.common.game.player.move;

import java.util.ArrayList;
import java.util.List;

import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;

public class MoveFromPoint {
	private final PlayerRelativeBoard playerBoard;
	private final MutableCheckerSet source;
	private int number;
	private final Roll roll;
	private final List<MoveChecker> moves;
	private final MoveValidator validator;

	public MoveFromPoint(PlayerRelativeBoard playerBoard, int number, Roll roll) {
		this(playerBoard, playerBoard.point(number), roll);
		this.number = number;
		if (roll.isDoubled()) {
			createDoubleMoves();
		} else {
			createMoves();
		}
	}

	public MoveFromPoint(PlayerRelativeBoard playerBoard, MutableCheckerSet source, Roll roll) {
		this.playerBoard = playerBoard;
		this.roll = roll;
		this.source = source;
		validator = new MoveValidator(playerBoard);
		moves = new ArrayList<>();
	}

	private void createDoubleMoves() {
		int distance = roll.low();
		int maxDistance = 4 * distance;
		MutableCheckerSet destination = getDestination(number - distance);
		MoveChecker move = addMove(source, destination);
		if (destination.isGoal()) {
			return;
		}

		while (distance > 0 && distance < maxDistance && move != null) {
			distance += roll.low();
			move = addNextDoubled(new MoveChecker(move), distance);
		}
	}

	private MutableCheckerSet getDestination(int number) {
		return number < 1 ? playerBoard.goal() : playerBoard.point(number);
	}

	private MoveChecker addNextDoubled(MoveChecker move, int value) {
		MutableCheckerSet src = move.getLastDestination();
		MutableCheckerSet dst = getDestination(number - value);
		if (!dst.isGoal() && validator.isValid(src, dst)) {
			MoveChecker next = new MoveChecker(src, dst);
			move.append(next);
			moves.add(move);
			return move;
		}
		return null;
	}

	private void createMoves() {
		MutableCheckerSet lowDestination = getDestination(number - roll.low());
		MutableCheckerSet highDestination = getDestination(number - roll.high());
		addMove(source, lowDestination);
		addMove(source, highDestination);
		addMove(source, highDestination, lowDestination);
	}

	private void addMove(MutableCheckerSet source, MutableCheckerSet highDestination, MutableCheckerSet lowDestination) {
		MutableCheckerSet sumDestination = getDestination(number - roll.sum());
		if (sumDestination == playerBoard.goal()) {
			return;
		}

		if (validator.isValid(source, highDestination)) {
			addPartialMove(source, highDestination, sumDestination);
		} else if (validator.isValid(source, lowDestination)) {
			addPartialMove(source, lowDestination, sumDestination);
		}
	}

	private void addPartialMove(MutableCheckerSet source, MutableCheckerSet middle, MutableCheckerSet destination) {
		MoveChecker move = new MoveChecker(source, middle);
		if (validator.isValid(middle, destination)) {
			move.makeNext(new MoveChecker(middle, destination));
			moves.add(move);
		}
	}

	private MoveChecker addMove(MutableCheckerSet source, MutableCheckerSet destination) {
		if (validator.isValid(source, destination)) {
			MoveChecker move = new MoveChecker(source, destination);
			moves.add(move);
			return move;
		}
		return null;
	}

	public List<MoveChecker> moves() {
		return moves;
	}

	public boolean isEmpty() {
		return moves.isEmpty();
	}
}
