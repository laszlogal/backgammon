package com.backgammon.common.game.board.setup;

import com.backgammon.common.game.board.PlayerRelativeBoard;

public interface CheckerSetup {
	void run(PlayerRelativeBoard board);
}
