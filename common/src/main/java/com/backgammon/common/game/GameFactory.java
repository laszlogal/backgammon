package com.backgammon.common.game;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.point.BoardController;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.ShuffleDice;
import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.util.BGLogger;

public interface GameFactory {
	void createGui(BoardView view);

	BGLogger createLogger();

	DrawBoard createDrawBoard();

	BoardView createView(Board board, DrawBoard drawBoard);

	void createDiceAnimation(ShuffleDice shuffleDice);

	BoardController createBoardController();

	void onGameStart();

	Runnable diceStart();

	BackgammonAgent playerAgent1();

	BackgammonAgent playerAgent2();

	InfoCenter infoCenter();
}
