package com.backgammon.common.game.player.move;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.backgammon.common.game.board.PlayerRelativeBoard;

public class MovesAvailable implements Iterable<MoveChecker>,
		Iterator<MoveChecker> {
	private final PlayerRelativeBoard playerBoard;
	private final Roll roll;
	private final List<MoveChecker> moves = new ArrayList<>();

	public MovesAvailable(PlayerRelativeBoard playerBoard, Roll roll) {
		this.playerBoard = playerBoard;
		this.roll = roll;

		if (roll.sum() > 0) {
			fillAvailableMoves();
		}
	}

	private void fillAvailableMoves() {
		if (hasCheckersInJail()) {
			enterBoard();
		} else {
			collectValidMoves();
		}
	}

	private boolean hasCheckersInJail() {
		return playerBoard.jail().count() != 0;
	}

	private void collectValidMoves() {
		for (int number = 24; number > 0; number--) {
			addValidMovesFrom(number);
		}
	}

	private void addValidMovesFrom(int number) {
		if (!playerBoard.owns(number)) {
			return;
		}
		MoveFromPoint movesFromPoint = new MoveFromPoint(playerBoard, number, roll);
		moves.addAll(movesFromPoint.moves());
	}

	private void enterBoard() {
		EnterFromJail enterFromJail = new EnterFromJail(playerBoard, roll);
		moves.addAll(enterFromJail.getMoves());
	}

	@Override
	public Iterator<MoveChecker> iterator() {
		return moves.iterator();
	}

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public MoveChecker next() {
		return iterator().next();
	}

	public List<MoveChecker> moves() {
		return moves;
	}

	public int size() {
		return moves.size();
	}

	public MoveChecker get(int index) {
		return moves.get(index);
	}

	public boolean hasMoves() {
		return size() != 0;
	}

	public PlayerRelativeBoard getPlayerBoard() {
		return playerBoard;
	}

	public boolean contains(MoveChecker move) {
		return moves.contains(move);
	}
}
