package com.backgammon.common.game.player.move;

import java.util.Objects;

import com.backgammon.common.game.board.point.BarPoint;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;

public class MoveToBar extends MoveChecker {
	private final BarPoint jail;

	public MoveToBar(MutableCheckerSet source, BarPoint jail) {
		super(source, jail);
		this.jail = jail;
	}

	@Override
	public void execute() {
		jail.increment();
	}

	@Override
	public int distance() {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		MoveToBar moveToBar = (MoveToBar) o;
		return Objects.equals(jail, moveToBar.jail);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), jail);
	}
}
