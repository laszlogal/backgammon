package com.backgammon.common.game.board.point;

import com.backgammon.common.game.board.point.checker.MutableCheckerSet;

public interface PointInterface extends MutableCheckerSet {
}
