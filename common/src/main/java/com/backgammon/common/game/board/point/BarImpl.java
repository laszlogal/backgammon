package com.backgammon.common.game.board.point;

import com.backgammon.common.game.board.point.checker.Checker;

public class BarImpl implements Bar {
	private final BarPoint hits;
	private final BarPoint jail;

	public BarImpl(BarPoint hits, BarPoint jail) {
		this.hits = hits;
		this.jail = jail;
	}

	@Override
	public BarPoint hits(Checker checker) {
		return hits.checker() == checker ? jail : hits;
	}

	@Override
	public BarPoint jail(Checker checker) {
		return jail.checker() == checker ? jail : hits;
	}
}
