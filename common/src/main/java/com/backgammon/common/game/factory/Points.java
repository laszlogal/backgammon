package com.backgammon.common.game.factory;

import com.backgammon.common.game.board.point.BarPoint;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.Checker;

public class Points {

	public static final int BAR_INDEX = 25;

	public static Point whitePoint(int count) {
		return point(Checker.WHITE, count);
	}

	public static Point point(Checker checker, int count) {
		return new Point(checker, count);
	}

	public static Point blackPoint(int count) {
		return point(Checker.BLACK, count);
	}

	public static Point emptyPoint(int index) {
		Point point = point(Checker.NONE, 0);
		point.setIndex(index);
		return point;
	}

	public static BarPoint whiteBar(int count) {
		return barPoint(Checker.WHITE, count, BAR_INDEX);
	}

	public static BarPoint blackBar(int count) {
		return barPoint(Checker.BLACK, count, BAR_INDEX);
	}

	public static BarPoint barPoint(Checker checker, int count, int index) {
		Point point = point(checker, count);
		return new BarPoint(point, index);
	}

	private Points() {

	}
}