package com.backgammon.common.game.draw;

import com.backgammon.common.bgraphics.BGraphics;
import com.backgammon.common.util.Log;

import java.util.HashMap;
import java.util.Map;

public class DrawDices {
	public static final String DICE_COLOR = "white";
	public static final int PLOT_RATIO = 12;
	private final BoardMetrics metrics;
	private BGraphics g;
	private int plot;
	private final Map<Integer, Integer> values;
	private String fgColor;

	public DrawDices(BoardMetrics metrics, BGraphics graphics) {
		this.metrics = metrics;
		g = graphics;
		
		values = new HashMap<>();
	}

	public void draw(int index, int value) {
		if (value == 0) {
			value = values.get(index);
			fgColor = "#cecece";
		} else {
			values.put(index, value);
			fgColor = "black";
		}

		int x = metrics.diceX() + (metrics.diceSize() + 20) * index;
		drawDice(x, metrics.diceY(), metrics.diceSize(), value);
	}

	private void drawDice(int x, int y, int side, int value) {
		g.setFillColor(DICE_COLOR);
		g.fillRect(x, y, side, side);
		g.setLineWidth(2);
		g.strokeRect(x, y, side, side);
		plot = side / PLOT_RATIO;
		drawVaue(x, y, side, value);
	}

	private void drawVaue(int x, int y, int side, int value) {
		g.setFillColor(fgColor);
		g.setLineColor(fgColor);
		switch (value) {
			case 1:
				drawOne(x, y, side);
				break;
			case 2:
				drawTwo(x, y, side);
				break;
			case 3:
				drawThree(x, y, side);
				break;
			case 4:
				drawFour(x, y, side);
				break;
			case 5:
				drawFive(x, y, side);
				break;
			case 6:
				drawSix(x, y, side);
				break;
			default:
				Log.debug("Should never happen");
		}
	}


	private void drawOne(int x, int y, int side) {
		g.fillCircle(x + side / 2, y + side / 2, plot);
	}

	private void drawTwo(int x, int y, int side) {
		g.fillCircle(x + side / 4, y + side / 4, plot);
		g.fillCircle(x + 3*side / 4, y + 3*side / 4, plot);
	}

	private void drawThree(int x, int y, int side) {
		drawOne(x, y, side);
		drawTwo(x, y, side);
	}

	private void drawFour(int x, int y, int side) {
		g.fillCircle(x + side / 4, y + side / 4, plot);
		g.fillCircle(x + side / 4, y + 3*side / 4, plot);
		g.fillCircle(x + 3* side / 4, y + side / 4, plot);
		g.fillCircle(x + 3* side / 4, y + 3*side / 4, plot);
	}

	private void drawFive(int x, int y, int side) {
		drawOne(x, y, side);
		drawFour(x, y, side);
	}

	private void drawSix(int x, int y, int side) {
		drawFour(x, y, side);
		g.fillCircle(x + side / 4, y + side / 2, plot);
		g.fillCircle(x + 3 * side / 4, y + side / 2, plot);
	}


	public void clear() {
		g.clear(metrics.diceX() - 2, metrics.diceY() - 2, 2 * metrics.diceSize() + 24
				, metrics.diceY() + metrics.diceSize() + 2);
	}
}
