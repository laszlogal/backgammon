package com.backgammon.common.game.draw;

public interface DrawInfo {
	void drawWinner(String name);
}
