package com.backgammon.common.game.board.setup;

import com.backgammon.common.game.board.PlayerRelativeBoard;

public class BearOffSetup implements CheckerSetup {
	@Override
	public void run(PlayerRelativeBoard board) {
		board.set(6, 2);
		board.set(5, 2);
		board.set(4, 2);
		board.set(3, 5);
		board.set(2, 2);
		board.set(1, 2);
	}
}
