package com.backgammon.common.game;

public interface InfoCenter {
	void win(String name);
	void setText(String text);
}
