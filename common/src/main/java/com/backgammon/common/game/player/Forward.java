package com.backgammon.common.game.player;

import com.backgammon.common.game.board.point.checker.Checker;

public class Forward implements BoardAccess {

	@Override
	public int toIndex(int number) {
		return number - 1;
	}

	@Override
	public Checker checker() {
		return Checker.WHITE;
	}
}
