package com.backgammon.common.game.draw;

import com.backgammon.common.game.GameMaster;

import java.util.Random;

public class ShuffleDice {
	private final Random random;
	private final DrawBoard drawBoard;
	private int diceValue1;
	private int diceValue2;
	private GameMaster gameMaster;

	public ShuffleDice(DrawBoard drawBoard, long seed) {
		this.drawBoard = drawBoard;
		random = new Random(seed);
	}

	public ShuffleDice(DrawBoard drawBoard) {
		this.drawBoard = drawBoard;
		random = new Random();
	}

	public void next() {
		diceValue1 = rollDice1();
		diceValue2 = rollDice2();
		drawBoard.drawDices(diceValue1, diceValue2);
	}

	protected int rollDice1() {
		return randomDiceValue();
	}

	protected int rollDice2() {
		return randomDiceValue();
	}

	protected int randomDiceValue() {
		return random.nextInt(6) + 1;
	}

	public void processResult() {
		gameMaster.onRollEnd(diceValue1, diceValue2);
	}

	public void setGameMaster(GameMaster gameMaster) {
		this.gameMaster = gameMaster;
	}
}

