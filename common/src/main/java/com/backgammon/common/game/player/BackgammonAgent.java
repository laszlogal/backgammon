package com.backgammon.common.game.player;

import com.backgammon.common.game.player.move.MovesAvailable;

public interface BackgammonAgent {
	void chooseFrom(Turn turn, MovesAvailable available);
}
