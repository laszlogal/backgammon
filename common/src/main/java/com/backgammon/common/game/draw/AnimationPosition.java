package com.backgammon.common.game.draw;

import java.util.Objects;

class AnimationPosition {
	double x;
	double y;

	public AnimationPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		AnimationPosition position = (AnimationPosition) o;
		return isThreshold(x - position.x) &&
				isThreshold(y - position.y);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	private boolean isThreshold(double a) {
		return Math.abs(a) < 10;
	}
}
