package com.backgammon.common.game.draw.triangles;

import com.backgammon.common.bgraphics.BGraphics;

public class DrawDownTriangle implements DrawTriangle {
	private final BGraphics g;
	public DrawDownTriangle(BGraphics graphics) {
		this.g = graphics;
	}

	@Override
	public void draw(Triangle triangle, BGraphicsDecorator decorator) {
		g.save();
		g.beginPath();
		decorator.setColor();
		g.setLineWidth(1);
		g.moveTo(triangle.left, triangle.top);
		g.lineTo(triangle.right, triangle.top);
		g.lineTo(triangle.middle, triangle.height);
		g.lineTo(triangle.left, triangle.top);
		decorator.decorate();
		g.closePath();
		g.restore();
	}
}

