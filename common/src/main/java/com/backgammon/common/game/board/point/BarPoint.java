package com.backgammon.common.game.board.point;

import com.backgammon.common.game.board.point.checker.Checker;

public class BarPoint implements PointInterface {

	private final Point point;
	private final int barIndex;

	public BarPoint(Point point, int barIndex) {
		this.point = point;
		this.barIndex = barIndex;
	}

	@Override
	public Checker checker() {
		return point.checker();
	}

	@Override
	public int count() {
		return point.count();
	}

	@Override
	public int index() {
		return barIndex;
	}

	@Override
	public boolean isGoal() {
		return false;
	}

	@Override
	public boolean isBar() {
		return true;
	}

	@Override
	public void set(Checker checker, int count) {
		point.set(checker, count);
	}

	public void increment() {
		point.set(checker(), count() + 1);
	}
}
