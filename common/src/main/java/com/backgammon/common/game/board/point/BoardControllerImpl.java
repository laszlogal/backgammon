package com.backgammon.common.game.board.point;

import com.backgammon.common.game.draw.triangles.DrawPointTriangles;

public class BoardControllerImpl implements BoardController {
    private final DrawPointTriangles drawPointTriangles;

    public BoardControllerImpl(DrawPointTriangles drawPointTriangles) {
        this.drawPointTriangles = drawPointTriangles;
    }

    @Override
    public void highlight(int number) {
        drawPointTriangles.select(number);
    }

    @Override
    public void clearHighlight(int number) {
        drawPointTriangles.deselect(number);
    }

    @Override
    public int getHitPoint(int x, int y) {
        return drawPointTriangles.pointAt(x, y);
    }

    @Override
    public void clearSelection() {
        drawPointTriangles. clearSelection();
    }
}
