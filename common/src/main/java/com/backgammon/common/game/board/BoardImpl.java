package com.backgammon.common.game.board;

import java.util.List;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;

public class BoardImpl implements Board {
	private final List<Point> points;
	private final List<Goal> goals;
	private final Bar bar;

	public BoardImpl(List<Point> points, List<Goal> goals, Bar bar) {
		this.points = points;
		this.goals = goals;
		this.bar = bar;
	}

	@Override
	public List<Point> points() {
		return points;
	}


	@Override
	public Point point(int number) {
		return points.get(number - 1);
	}

	@Override
	public List<Goal> goals() {
		return goals;
	}

	@Override
	public Bar bar() {
		return bar;
	}
}
