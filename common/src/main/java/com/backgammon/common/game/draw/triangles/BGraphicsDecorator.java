package com.backgammon.common.game.draw.triangles;


public interface BGraphicsDecorator {
	void setColor();
	void decorate();
}
