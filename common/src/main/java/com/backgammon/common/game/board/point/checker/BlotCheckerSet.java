package com.backgammon.common.game.board.point.checker;

public class BlotCheckerSet implements MutableCheckerSet {

	private final MutableCheckerSet source;

	public BlotCheckerSet(MutableCheckerSet source) {
		this.source = source;
	}

	@Override
	public void set(Checker checker, int count) {
		// read only
	}

	@Override
	public Checker checker() {
		return source.checker().opposite();
	}

	@Override
	public int count() {
		return 1;
	}

	@Override
	public int index() {
		return source.index();
	}

	@Override
	public boolean isGoal() {
		return false;
	}

	@Override
	public boolean isBar() {
		return false;
	}
}
