package com.backgammon.common.game.player.move;

/**
 * Represents a roll of dices of the player
 */
public class Roll {

	private int remaining;
	private int low;
	private int high;

	public Roll(int value1, int value2) {
		this.low = Math.max(value1, value2);
		this.high = Math.min(value1, value2);
		this.remaining = isDoubled() ? 4 * value1 : sum();
	}

	public Roll(Roll roll) {
		this(roll.high, roll.low);
	}

	public int high() {
		return low;
	}

	public int low() {
		return high;
	}

	public boolean isDoubled() {
		return low == high;
	}

	public int sum() {
		return low + high;
	}

	@Override
	public String toString() {
		return "[" + low + "] [" + high + "]"
				+ (isDoubled() ? " x2" : "");
	}

	public void subtract(int distance) {
		if (isDoubled()) {
			subtractDoubled();
			return;
		}

		if (high == distance) {
			high = 0;
		} else if (low == distance) {
			low = 0;
		} else if (high + low == distance) {
			high = 0;
			low = 0;
		}
	}

	private void subtractDoubled() {
		remaining -= low;
		if (remaining == 0) {
			low = 0;
			high = 0;
		}
	}

	public void bearOff(int distance) {
		if (isDoubled()) {
			subtractDoubled();
			return;
		}

		if (high >= distance) {
			high = 0;
		} else if (low >= distance) {
			low = 0;
		}
	}
}
