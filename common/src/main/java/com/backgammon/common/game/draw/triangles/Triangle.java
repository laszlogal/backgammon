package com.backgammon.common.game.draw.triangles;

public class Triangle {
	final int left;
	final int top;
	final int right;
	final int width;
	final int height;
	final int middle;
	final int bottom;

	private final DrawTriangle drawTriangle;
	final int number;

	public Triangle(int number, int left, int top, int width, int height, DrawTriangle drawTriangle) {
		this.number = number;
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
		this.drawTriangle = drawTriangle;
		this.right = left + width;
		this.middle = left + width/2;
		this.bottom = top + height;
	}

	public boolean isHit(int x, int y) {
		return left < x && x < right
				&& top < y && y < bottom;
	}

	public void draw(BGraphicsDecorator decorator) {
		drawTriangle.draw(this, decorator);
	}
}
