package com.backgammon.common.game.draw;

import com.backgammon.common.bgraphics.BGraphics;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.point.checker.CheckerSet;
import com.backgammon.common.game.player.move.MoveChecker;

public class MoveAnimation {
	public static final int ERASER = 4;
	private final BGraphics gAnim;
	private final DrawCheckers drawCheckers;
	private final DrawChecker drawChecker;
	private static final double SPEED = 8.0;
	private CheckerSet source;
	private AnimationPosition currentPosition;
	private AnimationPosition sourcePosition;
	private AnimationPosition destinationPosition;
	private double dx;
	private double dy;
	private Checker checker;
	private boolean blot = false;


	public MoveAnimation(BGraphics gAnim, DrawCheckers drawCheckers, DrawChecker drawChecker) {
		this.gAnim = gAnim;
		this.drawCheckers = drawCheckers;
		this.drawChecker = drawChecker;
	}

	public void start() {
		currentPosition = new AnimationPosition(sourcePosition.x, sourcePosition.y);
		double length = Math.hypot(destinationPosition.x - sourcePosition.x,
				destinationPosition.y - sourcePosition.y);
		dx = (destinationPosition.x - sourcePosition.x) / length;
		dy = (destinationPosition.y - sourcePosition.y) / length;
		if (!blot || source.isBar()) {
			clearCheckerFromSource();
		}
	}

	private void clearCheckerFromSource() {
		if (source.isBar()) {
			drawCheckers.clearLastCheckerFromBar(source.checker(), source.count());
		} else {
			drawCheckers.clearLastChecker(source.index() + 1, source.count());
		}
	}

	public void tick() {
		clearChecker();
		moveChecker();
		drawChecker();
	}


	public void moveChecker() {
		currentPosition.x += dx * SPEED;
		currentPosition.y += dy * SPEED;
	}

	public void drawChecker() {
		drawChecker.draw((int) currentPosition.x, (int) currentPosition.y, checker, gAnim);
	}

	public void clearChecker() {
		int x = (int) Math.round(currentPosition.x);
		int y = (int) Math.round(currentPosition.y);
		drawChecker.clear(x, y, gAnim, ERASER);
	}

	public void setMove(MoveChecker move) {
		this.source = move.getSource();
		CheckerSet destination = move.getDestination();
		this.checker = source.checker();
		this.blot = move.isBlot();
		sourcePosition = positionOf(source, source.count() - 1);
		destinationPosition = positionOf(destination, destination.count());
	}

	private AnimationPosition positionOf(CheckerSet point, int count) {
		int index = point.index();
		CheckerPosition checkerPosition = point.isBar()
				? getHitPosition(checker, count)
				: drawCheckers.newCheckerPosition(getPointIndex(point, index), count);
		int x = checkerPosition.x();
		int y = checkerPosition.y() + count * checkerPosition.gap();
		return new AnimationPosition(x, y);
	}

	private int getPointIndex(CheckerSet point, int index) {
		return point.isGoal() ? point.index() : index + 1;
	}

	private CheckerPosition getHitPosition(Checker checker, int count) {
		return checker == Checker.WHITE
				? drawCheckers.getWhiteHitPosition(count)
				: drawCheckers.getBlackHitPosition(count);
	}

	public boolean isFinished() {
		return currentPosition.equals(destinationPosition);
	}
}