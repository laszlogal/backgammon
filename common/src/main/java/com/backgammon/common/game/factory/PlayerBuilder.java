package com.backgammon.common.game.factory;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.PlayerRelativeBoardImpl;
import com.backgammon.common.game.board.setup.CheckerSetup;
import com.backgammon.common.game.board.setup.StandardSetup;
import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.Backward;
import com.backgammon.common.game.player.BoardAccess;
import com.backgammon.common.game.player.Forward;
import com.backgammon.common.game.player.Player;
import com.backgammon.common.game.player.PlayerImpl;
import com.backgammon.common.game.player.Turn;

public class PlayerBuilder {

	private final Board board;
	private final BoardView view;
	private BoardAccess access;
	private BackgammonAgent agent;
	private CheckerSetup setup;

	public PlayerBuilder(Board board, BoardView view) {
		this.board = board;
		this.view = view;
	}

	public void setForward() {
		access = new Forward();
	}

	public void setBackward() {
		access = new Backward();
	}

	public Player getResult() {
		PlayerRelativeBoard relativeBoard = new PlayerRelativeBoardImpl(board, access, setup);
		Turn turn = new Turn(relativeBoard, view);
		String name = access.checker().toString();
		return new PlayerImpl(turn, name, agent);
	}

	public void setStandardSetup() {
		setup = new StandardSetup();
	}

	public void setAgent(BackgammonAgent agent) {
		this.agent = agent;
	}
}
