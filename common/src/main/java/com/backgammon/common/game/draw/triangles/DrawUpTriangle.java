package com.backgammon.common.game.draw.triangles;

import com.backgammon.common.bgraphics.BGraphics;

public class DrawUpTriangle implements DrawTriangle {
	private final BGraphics g;

	public DrawUpTriangle(BGraphics graphics) {
		this.g = graphics;
	}

	public void draw(Triangle triangle, BGraphicsDecorator decorator) {
		g.save();
		g.beginPath();
		decorator.setColor();
		g.setLineWidth(1);
		g.moveTo(triangle.middle, triangle.top);
		g.lineTo(triangle.left, triangle.bottom);
		g.lineTo(triangle.right, triangle.bottom);
		g.lineTo(triangle.middle, triangle.top);
		decorator.decorate();
		g.closePath();
		g.restore();
	}
}

