package com.backgammon.common.game.player;

import com.backgammon.common.game.board.point.checker.Checker;

public interface BoardAccess {
	int toIndex(int number);

	Checker checker();
}
