package com.backgammon.common.game.draw.triangles;

public interface NumberRunnable {
	void run(int number);
}
