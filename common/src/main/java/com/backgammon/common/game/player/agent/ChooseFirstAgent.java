package com.backgammon.common.game.player.agent;

import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.Turn;
import com.backgammon.common.game.player.move.MovesAvailable;

public class ChooseFirstAgent implements BackgammonAgent {

	@Override
	public void chooseFrom(Turn turn, MovesAvailable available) {
		turn.move(available.get(0));
	}
}
