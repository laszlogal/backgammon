package com.backgammon.common.game.draw.triangles;

public interface DrawTriangle {
	void draw(Triangle triangle, BGraphicsDecorator decorator);
}
