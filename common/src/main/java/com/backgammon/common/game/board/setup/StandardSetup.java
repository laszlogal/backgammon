package com.backgammon.common.game.board.setup;

import com.backgammon.common.game.board.PlayerRelativeBoard;

public class StandardSetup implements CheckerSetup {
	@Override
	public void run(PlayerRelativeBoard board) {
		board.set(24, 2);
		board.set(13, 5);
		board.set(8, 3);
		board.set(6, 5);
	}
}
