package com.backgammon.common.game.board.point;

import java.util.Objects;

import com.backgammon.common.game.board.point.checker.Checker;

public class Point implements PointInterface {
	private Checker checker;
	private int count;
	private int index = -1;

	public Point() {
		clear();
	}

	public Point(Checker checker, int count) {
		set(checker, count);
	}

	@Override
	public void set(Checker checker, int count) {
		this.checker = checker;
		this.count = count;
	}

	public void clear() {
		set(Checker.NONE, 0);
	}

	@Override
	public Checker checker() {
		return checker;
	}

	@Override
	public int count() {
		return count;
	}

	@Override
	public String toString() {
		return "Point  "
				+ (count == 0 ? "empty" : count + " of " + checker);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Point that = (Point) o;
		return count == that.count &&
				checker == that.checker &&
				index == that.index;
	}

	@Override
	public int hashCode() {
		return Objects.hash(checker, count);
	}

	@Override
	public int index() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public boolean isGoal() {
		return false;
	}

	@Override
	public boolean isBar() {
		return false;
	}
}
