package com.backgammon.common.game.player;

import com.backgammon.common.game.player.move.MoveChecker;

public interface MoveFinishedHandler {
	void onMoveFinished(MoveChecker move);
}
