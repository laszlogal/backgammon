package com.backgammon.common.game.board;

import com.backgammon.common.game.board.point.BarPoint;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.setup.CheckerSetup;
import com.backgammon.common.game.player.BoardAccess;

public interface PlayerRelativeBoard {
	/***
	 *
	 * @return the goal of the specified player
	 */
	Goal goal();

	/**
	 * Gives the hits the player made by removing
	 * a standalone checker of the opponent.
	 * Note that hits is the bar of the opponent.
	 *
	 * @return the hits of the specified player
	 */
	BarPoint hits();

	/**
	 * Gives the jail of player: it is the checkers
	 * hits by the opponent.
	 * Note that hits is the bar of the opponent.
	 *
	 * @return the hits of the specified player
	 */
	BarPoint jail();

	/**
	 * Gets a specified point from the player point of view.
	 *
	 * @param number number of the point.
	 * @return the point
	 */
	Point point(int number);

	/**
	 * @param number point to query
	 * @return if player owns this point.
	 */
	boolean owns(int number);

	/**
	 * @return checkers in goal
	 */
	int goalCount();

	void set(int number, int count);

	void setOpposite(int number, int count);

	void setup(CheckerSetup setup);

	boolean isGoalFilled();

	BoardAccess access();

	void makeAHit();
}
