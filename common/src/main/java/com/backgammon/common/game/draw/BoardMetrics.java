package com.backgammon.common.game.draw;

public class BoardMetrics {
	public static final int CHECKER_MARGIN = 14;
	private static final int LEFT = 0;
	private static final int TOP = 0;
	private int right;
	private int bottom;
	private int width;
	private int height;
	private int pointWidth;
	private int goalWidth;
	private int checkerRadius;
	private int middleX;
	private int middleY;
	private int boardBorderWidth;
	private int goalBorderWidth;

	public BoardMetrics(int width, int height) {
		update(width, height);
	}

    public BoardMetrics() {

    }

    public void update(int width, int height) {
		this.width = width;
		this.height = height;
		right = LEFT + width;
		bottom = TOP + height;
		pointWidth = width / 17;
		goalWidth = (int) (1.5 * pointWidth);
		checkerRadius = height / 26;
		middleX = LEFT + width/2;
		middleY = TOP + height/2;
	}

	public int getLeft() {
		return LEFT;
	}

	public int getTop() {
		return TOP;
	}

	public int getRight() {
		return right;
	}

	public int getBottom() {
		return bottom;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getPointWidth() {
		return pointWidth;
	}

	public int getGoalWidth() {
		return goalWidth;
	}

	public int getMiddleX() {
		return middleX;
	}

	public int getMiddleY() {
		return middleY;
	}

	public int getCheckerRadius() {
		return checkerRadius;
	}


	public int getUpperPointX(int number) {
		if (number < 7) {
			return right - goalWidth - 8 - (number * pointWidth);
		} else {
			return LEFT + goalWidth  + 8 + ((12 - number) * pointWidth);
		}
	}

	public int getBottomPointX(int number) {
		int n = number - 13;
		if (number < 19) {
			return LEFT + (goalWidth + 8 + n * pointWidth);
		} else {
			return right - goalWidth - 8 - ((25 - number) * pointWidth);
		}
	}

	public int getUpperPointY() {
		return TOP + CHECKER_MARGIN  + checkerRadius;
	}

	public int getBottomPointY() {
		return bottom - CHECKER_MARGIN - checkerRadius;
	}

	public int getBoardBorderWidth() {
		return boardBorderWidth;
	}

	public void setBoardBorderWidth(int boardBorderWidth) {
		this.boardBorderWidth = boardBorderWidth;
	}

	public int getGoalBorderWidth() {
		return goalBorderWidth;
	}

	public void setGoalBorderWidth(int goalBorderWidth) {
		this.goalBorderWidth = goalBorderWidth;
	}

	public int diceX() {
		return LEFT + 10 * getGoalBorderWidth();
	}

	public int diceY() {
		return (height - diceSize()) / 2;
	}

	public int diceSize() {
		return 2 * checkerRadius;
	}

	public int triangleMiddle() {
		return pointWidth / 2;
	}
}
