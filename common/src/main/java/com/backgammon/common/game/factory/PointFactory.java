package com.backgammon.common.game.factory;

import static com.backgammon.common.game.factory.Points.*;

import java.util.ArrayList;
import java.util.List;

import com.backgammon.common.game.board.point.Point;

public class PointFactory {

	public static final int POINT_COUNT_OF_BOARD = 24;

	public static List<Point> standard() {
		List<Point> points = empty();
		putTo(1, points, whitePoint(2));
		putTo(6, points, blackPoint(5));
		putTo(8, points, blackPoint(3));
		putTo(12, points, whitePoint(5));
		putTo(13, points, blackPoint(5));
		putTo(17, points, whitePoint(3));
		putTo(19, points, whitePoint(5));
		putTo(24, points, blackPoint(2));
		return points;
	}

	public static List<Point> bearOff() {
		List<Point> points = empty();
		putTo(23, points, blackPoint(5));
		putTo(20, points, blackPoint(5));
		putTo(18, points, blackPoint(5));
		return points;
	}

	private static void putTo(int number, List<Point> points, Point point) {
		int index = number - 1;
		point.setIndex(index);
		points.set(index, point);
	}

	public static List<Point> empty() {
		List<Point> list = new ArrayList<>();
		for (int i = 0; i < POINT_COUNT_OF_BOARD; i++) {
			list.add(emptyPoint(i));
		}
		return list;
	}

	private PointFactory() {

	}
}
