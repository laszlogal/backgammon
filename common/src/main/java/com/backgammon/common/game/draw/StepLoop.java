package com.backgammon.common.game.draw;

import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;

public interface StepLoop extends AnimListener {
	void setMove(MoveChecker move);

	void start();

	void setHandler(MoveFinishedHandler handler);
}
