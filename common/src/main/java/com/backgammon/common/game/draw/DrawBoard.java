package com.backgammon.common.game.draw;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.CheckerSet;

public interface DrawBoard {
	void drawBackground();

	void drawPoint(Point point);

	void drawGoal(CheckerSet goal);

	void resize(int width, int height);

	void drawBar(Bar bar);

	void drawDices(int value1, int value2);
}
