package com.backgammon.common.game.player;

import com.backgammon.common.game.GameMaster;
import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.checker.BlotCheckerSet;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.MoveToBar;
import com.backgammon.common.game.player.move.MovesAvailable;
import com.backgammon.common.game.player.move.Roll;
import com.backgammon.common.util.Log;

public class Turn {
	private final PlayerRelativeBoard board;
	private final BoardView view;
	private final Subtractor subtractor;
	private BackgammonAgent agent;
	private Roll roll;
	private final MoveFinishedImpl moveFinishedHandler;
	private boolean blot;
	private boolean inTurn = false;
	private GameMaster gameMaster;

	public Turn(PlayerRelativeBoard board, BoardView view) {
		this.board = board;
		this.view = view;
		moveFinishedHandler = new MoveFinishedImpl(this, view);
		subtractor = new Subtractor(board);
	}

	public void start(Roll roll) {
		this.roll = roll;
		inTurn = true;
		logTurnStart();
		nextMoveFromRoll();
	}

	private void logTurnStart() {
		Log.debug(board.access().checker() + ": " + roll.toString());
	}

	void nextMoveFromRoll() {
		MovesAvailable movesAvailable = new MovesAvailable(board, roll);
		if (movesAvailable.hasMoves()) {
			agent.chooseFrom(this, movesAvailable);
		} else {
			endTurn();
		}
	}

	public boolean isWinner() {
		return board.isGoalFilled();
	}

	public void move(MoveChecker move) {
		blot = move.isBlot();
		subtractor.subtract(roll, move);
		view.setRoll(roll);
		view.update();
		view.animateMove(move, moveFinishedHandler);
	}

	public void setAgent(BackgammonAgent agent) {
		this.agent = agent;
	}

	public void nextMove(MoveChecker move) {
		if (blot) {
			move.makeNext(
					new MoveToBar(new BlotCheckerSet(move.getDestination()),
							board.hits()));
		}

		if (move.hasNext()) {
			move(move.next());
		} else {
			nextMoveFromRoll();
		}

	}

	public void endTurn() {
		inTurn = false;
		gameMaster.endTurn();
	}

	public boolean isInTurn() {
		return inTurn;
	}

	public void setGameMaster(GameMaster gameMaster) {
		this.gameMaster = gameMaster;
	}

	public GameMaster getGameMaster() {
		return gameMaster;
	}
}