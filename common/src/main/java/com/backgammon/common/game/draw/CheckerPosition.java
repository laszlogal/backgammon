package com.backgammon.common.game.draw;

public class CheckerPosition {
	private final int x;
	private final int y;
	private final int gap;

	public CheckerPosition(int x, int y, int gap) {
		this.x = x;
		this.y = y;
		this.gap = gap;
	}

	public int x() {
		return x;
	}

	public int y() {
		return y;
	}

	public int gap() {
		return gap;
	}
}
