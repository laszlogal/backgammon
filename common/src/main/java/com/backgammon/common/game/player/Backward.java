package com.backgammon.common.game.player;

import com.backgammon.common.game.board.point.checker.Checker;

public class Backward implements BoardAccess {

	public static final int MAX_POINT_NUMBER = 24;

	@Override
	public int toIndex(int number) {
		return MAX_POINT_NUMBER - number;
	}

	@Override
	public Checker checker() {
		return Checker.BLACK;
	}
}
