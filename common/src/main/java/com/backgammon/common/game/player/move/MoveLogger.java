package com.backgammon.common.game.player.move;

import com.backgammon.common.game.board.point.checker.CheckerSet;

public class MoveLogger {

	private final MoveChecker move;
	private StringBuilder sb;

	public MoveLogger(MoveChecker move) {
		this.move = move;
	}

	public String log() {
		sb = new StringBuilder();
		sb.append(move.distance());
		sb.append(": (");
		append(move.getSource());
		sb.append(" -> ");
		append(move.getDestination());
		sb.append(")");
		if (move.isBlot()) {
			sb.append(" BLOT");
		}
		if (move.hasNext()) {
			sb.append(" =>");
		}
		return sb.toString();
	}

	private void append(CheckerSet info) {
		String id;
		if (info.isGoal()) {
			id = "goal";
		} else if (info.isBar()) {
			id = "bar";
		} else {
			id = info.index() + 1 + "";
		}
		sb.append(id);
	}
}
