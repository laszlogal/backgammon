package com.backgammon.common.game.board.point;

public interface BoardController {
    void highlight(int number);

    void clearHighlight(int number);

    int getHitPoint(int x, int y);

    void clearSelection();
}
