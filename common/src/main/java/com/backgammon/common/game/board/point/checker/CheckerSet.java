package com.backgammon.common.game.board.point.checker;

public interface CheckerSet {
	Checker checker();

	int count();

	int index();

	boolean isGoal();

	boolean isBar();
}
