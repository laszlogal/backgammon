package com.backgammon.common.game.player.agent;

import java.util.Random;

import com.backgammon.common.game.player.BackgammonAgent;
import com.backgammon.common.game.player.Turn;
import com.backgammon.common.game.player.move.MovesAvailable;

public class RandomMovesAgent implements BackgammonAgent {

	private final Random random;

	public RandomMovesAgent() {
		random = new Random();
	}

	@Override
	public void chooseFrom(Turn turn, MovesAvailable available) {
		int index = random.nextInt(available.size());
		turn.move(available.get(index));
	}
}
