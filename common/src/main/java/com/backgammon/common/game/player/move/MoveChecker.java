package com.backgammon.common.game.player.move;

import java.util.Objects;

import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;

public class MoveChecker {
	private final MutableCheckerSet source;
	private final MutableCheckerSet destination;
	private MoveChecker next = null;
	private final MoveLogger logger;


	public MoveChecker(MoveChecker move) {
		this(move.source, move.destination);
		next = move.next != null ? new MoveChecker(move.next) : null;
	}

	public MoveChecker(MutableCheckerSet source, MutableCheckerSet destination) {
		this.source = source;
		this.destination = destination;
		logger = new MoveLogger(this);
	}

	public MutableCheckerSet getSource() {
		return source;
	}

	public MutableCheckerSet getDestination() {
		return destination;
	}

	public boolean isBlot() {
		if (destination.isBar()) {
			return false;
		}
		return destination.count() == 1
				&& source.checker() == destination.checker().opposite();
	}

	public void execute() {
		if (!destination.isBar() && (isDestinationEmpty() || isBlot())) {
			destination.set(source.checker(), 1);
		} else {
			addChecker(destination);
		}
		removeChecker(source);
	}

	private void removeChecker(MutableCheckerSet source) {
		int count = source.count() - 1;
		if (source.isBar()) {
			source.set(source.checker(), count);
		} else {
			source.set(count > 0 ? source.checker() : Checker.NONE, count);
		}
	}

	private void addChecker(MutableCheckerSet destination) {
		destination.set(source.checker(), destination.count() + 1);
	}

	private boolean isDestinationEmpty() {
		return destination.count() == 0;
	}

	public int distance() {
		return Math.abs(leaf().destination.index() - source.index());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		MoveChecker that = (MoveChecker) o;
		return Objects.equals(source, that.source) &&
				Objects.equals(destination, that.destination)
				&& Objects.equals(next, that.next);
	}

	@Override
	public int hashCode() {
		return Objects.hash(source, destination);
	}

	@Override
	public String toString() {
		return logger.log();
	}

	public MoveChecker makeNext(MoveChecker next) {
		this.next = next;
		return this;
	}

	public boolean hasNext() {
		return next != null;
	}

	public MoveChecker next() {
		return next;
	}

	public void append(MoveChecker next) {
		leaf().makeNext(next);
	}

	public MutableCheckerSet getLastDestination() {
		return leaf().getDestination();
	}

	private MoveChecker leaf() {
		MoveChecker node = this;
		while (node.hasNext()) {
			node = node.next;
		}
		return node;
	}
}
