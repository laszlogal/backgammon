package com.backgammon.common.game.board.point.checker;

public interface MutableCheckerSet extends CheckerSet {
	void set(Checker checker, int count);
}
