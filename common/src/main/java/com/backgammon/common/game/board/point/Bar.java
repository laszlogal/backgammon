package com.backgammon.common.game.board.point;

import com.backgammon.common.game.board.point.checker.Checker;

public interface Bar {
	BarPoint hits(Checker access);

	BarPoint jail(Checker access);
}
