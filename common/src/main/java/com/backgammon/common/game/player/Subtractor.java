package com.backgammon.common.game.player;

import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.checker.CheckerSet;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.Roll;

public class Subtractor {
	private final PlayerRelativeBoard board;

	public Subtractor(PlayerRelativeBoard board) {
		this.board = board;
	}

	public void subtract(Roll roll, MoveChecker move) {
		int distance = distance(move);
		if (move.getDestination().isGoal()) {
			roll.bearOff(distance);
		} else {
			roll.subtract(distance);
		}
	}

	private int distance(MoveChecker move) {
		if (move.getSource().isBar()) {
			return 24 - getNumber(move.getDestination());
		}
		int destination = move.getDestination().isGoal()
				? 1
				: getNumber(move.getDestination());
		return Math.abs(destination - getNumber(move.getSource()));
	}

	private int getNumber(CheckerSet checkers) {
		return board.access().toIndex(checkers.index() + 1);
	}
}
