package com.backgammon.common.game;

import java.util.List;

import com.backgammon.common.game.board.BoardView;
import com.backgammon.common.game.draw.ShuffleDice;
import com.backgammon.common.game.player.Player;
import com.backgammon.common.game.player.move.Roll;

public class GameMasterImp implements GameMaster {

	private final List<Player> players;
	private final InfoCenter infoCenter;
	private final Runnable diceStart;
	private Player currentPlayer;
	private final BoardView view;

	public GameMasterImp(ShuffleDice shuffleDice, BoardView view,
						 List<Player> players,
						 InfoCenter infoCenter,
						 Runnable diceStart) {
		this.view = view;
		this.players = players;
		this.infoCenter = infoCenter;
		this.diceStart = diceStart;
		players.forEach(player -> player.setGameMaster(this));
		shuffleDice.setGameMaster(this);
	}

	@Override
	public void newGame() {
		nextPlayersTurn();
	}

	private void  rollDices() {
		diceStart.run();
	}

	private void switchPlayer() {
		if (currentPlayer == player1() && player2() != null) {
			currentPlayer = player2();
		} else {
			currentPlayer = player1();
		}
		says(currentPlayer + " is next");
	}

	private Player player1() {
		return players.get(0);
	}

	private Player player2() {
		return players.get(1);
	}

	@Override
	public void onRollEnd(int diceValue1, int diceValue2) {
		Roll roll = new Roll(diceValue1, diceValue2);
		view.setRoll(roll);
		currentPlayer.startTurn(roll);
	}

	@Override
	public void says(String message) {
		infoCenter.setText(message);
	}

	@Override
	public void update() {
		view.update();
	}

	@Override
	public void endTurn() {
		if (currentPlayer.isWinner()) {
			win();
		} else {
			nextPlayersTurn();
		}
	}

	private void nextPlayersTurn() {
		switchPlayer();
		rollDices();
	}

	private void win() {
		infoCenter.win(currentPlayer + "");
	}
}