package com.backgammon.common.game.board;

import java.util.Collection;
import java.util.List;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.draw.StepLoop;
import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.Roll;

public class BoardViewImpl implements BoardView {
	private final DrawBoard drawBoard;
	private final List<Point> points;
	private final Collection<Goal> goals;
	private final Bar bar;
	private final StepLoop step;
	private Roll roll;

	public BoardViewImpl(Board board, DrawBoard drawBoard, StepLoop step) {
		this.drawBoard = drawBoard;
		points = board.points();
		goals = board.goals();
		bar = board.bar();
		this.step = step;
	}

	@Override
	public void update() {
		draw();
	}

	private void draw() {
		drawBoard.drawBackground();
		drawPoints();
		drawGoals();
		drawBar();
		drawDices();
	}

	private void drawDices() {
		if (roll == null) {
			return;
		}
		updateDices(roll);
	}
	@Override
	public void updateDices(Roll roll) {
		drawBoard.drawDices(roll.high(), roll.low());
	}

	private void drawBar() {
		drawBoard.drawBar(bar);
	}

	private void drawGoals() {
		goals.forEach(drawBoard::drawGoal);
	}

	private void drawPoints() {
		for (Point point : points) {
			drawBoardPoint(point);
		}
	}

	private void drawBoardPoint(Point point) {
		drawBoard.drawPoint(point);
	}

	@Override
	public void resize(int width, int height) {
		drawBoard.resize(width, height);
		draw();
	}

	@Override
	public void animateMove(MoveChecker move, MoveFinishedHandler handler) {
		step.setMove(move);
		step.setHandler(handler);
		step.start();
	}

	@Override
	public void setRoll(Roll roll) {
		this.roll = new Roll(roll);
	}

}