package com.backgammon.common.game.draw;

import com.backgammon.common.game.board.BoardComponents;
import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.CheckerSet;
import com.backgammon.common.game.draw.triangles.DrawPointTriangles;

public class DrawBoardCommon implements DrawBoard {

    private final BoardMetrics metrics;
    private final DrawBackground background;
    private final DrawPointTriangles triangles;
    private final DrawCheckers checkers;
    private final DrawDices dices;

    public DrawBoardCommon(BoardComponents parts) {
        metrics = parts.getMetrics();
        background = parts.getBackground();
        triangles = parts.getDrawPointTriangles();
        checkers = parts.getCheckers();
        dices = parts.getDices();
    }

    @Override
    public void drawBackground() {
        background.draw(metrics);
    }

    @Override
    public void drawPoint(Point point) {
        int number = point.index() + 1;
        triangles.draw(number);
        triangles.drawSelection();
        checkers.drawPoint(number, point);
    }

    @Override
    public void drawGoal(CheckerSet goal) {
        checkers.drawGoal(goal);
    }

    @Override
    public void resize(int width, int height) {
        metrics.update(width, height);
        triangles.resize();
        checkers.update(metrics);

    }

    @Override
    public void drawBar(Bar bar) {
        checkers.drawBar(bar);
    }

    @Override
    public void drawDices(int value1, int value2) {
        dices.draw(0, value1);
        dices.draw(1, value2);
    }
}
