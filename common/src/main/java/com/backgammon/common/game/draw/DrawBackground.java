package com.backgammon.common.game.draw;

import com.backgammon.common.bgraphics.BGraphics;

public class DrawBackground {
	public static final String BOARD_FILL_COLOR = "#fff7e0";
	private final BGraphics g;

	public DrawBackground(BGraphics graphics) {
		this.g = graphics;
	}

	public void draw(BoardMetrics metrics) {
		g.setLineWidth(1);
		drawBoardBase(metrics.getTop(), metrics.getLeft(),
				metrics.getWidth(), metrics.getHeight(),
				metrics.getBoardBorderWidth());

		drawGoalBorder(metrics.getLeft(),
				metrics.getGoalWidth(),
				metrics.getTop(), metrics.getBottom(), metrics.getGoalBorderWidth());


		drawBoardMiddle(metrics);

		drawGoalBorder(metrics.getLeft() + metrics.getWidth(),
				-metrics.getGoalWidth(),
				metrics.getTop(),
				metrics.getBottom(), metrics.getGoalBorderWidth());
	}

	private void drawBoardMiddle(BoardMetrics metrics) {
		int pointWidth = metrics.getPointWidth();
		g.setFillColor("black");
		g.fillRect(metrics.getLeft() + (metrics.getWidth() - pointWidth)/2,
				metrics.getTop(), pointWidth, metrics.getHeight());
	}

	private void drawBoardBase(int top, int left, int width, int height, int borderWidth) {
		g.setFillColor(BOARD_FILL_COLOR);
		g.fillRect(left, top, width, height);
		g.setFillColor("black");
		g.setLineWidth(borderWidth);
		g.strokeRect(left, top, width, height);
	}

	private void drawGoalBorder(int left, int goalWidth, int top, int bottom, int goalBorderWidth) {
		g.save();
		g.setLineWidth(goalBorderWidth);
		g.drawLine(left + goalWidth, top,left + goalWidth, bottom);
		g.restore();
	}
}