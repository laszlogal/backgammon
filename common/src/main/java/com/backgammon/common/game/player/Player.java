package com.backgammon.common.game.player;

import com.backgammon.common.game.GameMaster;
import com.backgammon.common.game.player.move.Roll;

public interface Player {
	void setGameMaster(GameMaster gameMaster);
	void startTurn(Roll roll);
	boolean isWinner();
}