package com.backgammon.common.game.board;

import java.util.List;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;

public interface Board {

	/**
	 * @return all Point of the game
	 */
	List<Point> points();

	/**
	 * Gets a specified at its absolute number.
	 *
	 * @param number number of the point.
	 * @return the point
	 */
	Point point(int number);

	/**
	 * @return list of goals
	 */
	List<Goal> goals();


	/**
	 *
	 * @return the middle bar of the board
	 * for hits/jail.
	 */
	Bar bar();
}
