package com.backgammon.common.game.player.move;

import java.util.ArrayList;
import java.util.List;

import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.BarPoint;
import com.backgammon.common.game.board.point.checker.MutableCheckerSet;

public class EnterFromJail {
	private final MoveValidator validator;
	private final PlayerRelativeBoard playerBoard;
	private final Roll roll;
	private final BarPoint jail;
	private final List<MoveChecker> moves;

	public EnterFromJail(PlayerRelativeBoard playerBoard, Roll roll) {
		this.playerBoard = playerBoard;
		this.roll = roll;
		this.jail = playerBoard.jail();
		moves = new ArrayList<>();
		validator = new MoveValidator(playerBoard);
		createMoves();

	}

	private void createMoves() {
		enterWith(roll.low());
		enterWith(roll.high());
	}

	private void enterWith(int diceValue) {
		if (diceValue == 0) {
			return;
		}
		MutableCheckerSet destination = playerBoard.point(enterWithValue(diceValue));
		if (validator.isValid(jail, destination)) {
			moves.add(new MoveChecker(jail, destination));
		}
	}

	private int enterWithValue(int diceValue) {
		return (24 - diceValue) + 1;
	}


	public List<MoveChecker> getMoves() {
		return moves;
	}
}
