package com.backgammon.common.game.board.point.checker;

public enum Checker {
	BLACK("black", "#777777"),
	WHITE("white", "black"),
	NONE();

	private final String color;
	private final String borderColor;

	Checker() {
		color = "";
		borderColor = "";
	}

	Checker(String color, String borderColor) {
		this.color = color;
		this.borderColor = borderColor;
	}

	public String getColor() {
		return color;
	}


	public String getBorderColor() {
		return borderColor;
	}

	public Checker opposite() {
		if (this == Checker.NONE) {
			return this;
		}
		return this == Checker.WHITE ? Checker.BLACK : Checker.WHITE;
	}
}
