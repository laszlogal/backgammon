package com.backgammon.common.game.draw;

public interface AnimListener {
	void run();

	void onEnd();
}
