package com.backgammon.common.util;

import java.util.logging.Logger;

public class JBGLogger implements BGLogger {

	private final Logger logger = Logger.getAnonymousLogger();

	@Override
	public void debug(String message) {
		logger.info(message);
	}

	@Override
	public void info(String message) {
		debug(message);
	}
}
