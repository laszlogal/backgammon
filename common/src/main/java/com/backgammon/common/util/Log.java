package com.backgammon.common.util;

public class Log {
	private static BGLogger bgLogger = null;

	public static void setLogger(BGLogger bgLogger) {
		Log.bgLogger = bgLogger;
	}

	public static void debug(String message) {
		if (bgLogger != null) {
			bgLogger.debug(message);
		}
	}

	public static void info(String message) {
		if (bgLogger != null) {
			bgLogger.info(message);
		}
	}

	private Log() {

	}
}
