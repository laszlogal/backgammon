package com.backgammon.common.util;

public interface InfoDisplay {
    void info(String text);
}
