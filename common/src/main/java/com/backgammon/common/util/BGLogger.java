package com.backgammon.common.util;

public interface BGLogger {
	void debug(String message);
	void info(String message);
}
