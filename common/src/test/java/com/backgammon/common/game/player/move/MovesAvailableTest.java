package com.backgammon.common.game.player.move;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.PlayerRelativeBoardImpl;
import com.backgammon.common.game.board.setup.BearOffSetup;
import com.backgammon.common.game.board.setup.CheckerSetup;
import com.backgammon.common.game.board.setup.StandardSetup;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.factory.PointFactory;
import com.backgammon.common.game.player.Backward;
import com.backgammon.common.game.player.Forward;

public class MovesAvailableTest {

	private PlayerRelativeBoard playerBoard;
	private PlayerRelativeBoardImpl playerBoardOpposite;

	@Before
	public void setUp() {
		Board board = new BoardFactory().withPoints(PointFactory.empty()).build();
		playerBoard = new PlayerRelativeBoardImpl(board, new Forward());
		playerBoardOpposite = new PlayerRelativeBoardImpl(board, new Backward());
	}

	@Test
	public void testMovesAvailableWith1And2() {
		withStandardSetup();
		MovesAvailable available = new MovesAvailable(playerBoard, new Roll(1, 2));
		assertThat(available.moves(), containsInAnyOrder(
				move(24, 23),
				move(24, 22),
				move(24, 21),
				move(13, 11),
				move(13, 10),
				move(8, 7),
				move(8, 6),
				move(8, 5),
				move(6, 5),
				move(6, 4),
				move(6, 3)
		));
	}

	private void withStandardSetup() {
		withSetup(new StandardSetup());
	}

	private void withSetup(CheckerSetup setup) {
		setup.run(playerBoard);
		setup.run(playerBoardOpposite);
	}

	@Test
	public void testBearOffWith3And4() {
		withBearOffSetup();
		MovesAvailable available = new MovesAvailable(playerBoard, new Roll(3, 4));
		assertThat(available.moves(), containsInAnyOrder(
				move(6, 3),
				move(6, 2),
				move(5, 2),
				move(5, 1),
				move(4, 1),
				moveToGoal(4),
				moveToGoal(3),
				moveToGoal(2),
				moveToGoal(1)
		));
	}

	@Test
	public void testEqualityMoveToGoal() {
		MoveChecker m1 = moveToGoal(1);
		MoveChecker m2 = moveToGoal(2);
		assertNotEquals(m1, m2);
	}

	private MoveChecker moveToGoal(int source) {
		return new MoveChecker(playerBoard.point(source), playerBoard.goal());
	}

	private void withBearOffSetup() {
		withSetup(new BearOffSetup());
	}

	private MoveChecker move(int source, int destination) {
		return new MoveChecker(playerBoard.point(source),
				playerBoard.point(destination));
	}
}