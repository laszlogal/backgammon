package com.backgammon.common.game.player;

import com.backgammon.common.game.GameMaster;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.MovesAvailable;

public class AgentMock implements BackgammonAgent {

	public AgentMock(Turn turn) {
	}

	@Override
	public void chooseFrom(Turn turn, MovesAvailable available) {
		MoveChecker chosenMove = available.get(0);
		turn.move(chosenMove);
	}

	@Override
	public void onTurnStart() {

	}

	@Override
	public void setAccess(BoardAccess access) {

	}

	@Override
	public void setGameMaster(GameMaster gameMaster) {

	}
}
