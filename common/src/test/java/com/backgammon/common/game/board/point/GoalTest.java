package com.backgammon.common.game.board.point;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.factory.BoardFactory;

public class GoalTest {
	private final Point point = new Point(Checker.WHITE, 0);
	private final Goal goalWhite = new Goal(BoardFactory.WHITE_GOAL_INDEX,
			point);

	@Test
	public void testConstructor() {
		assertTrue(goalContains(Checker.WHITE, 0));
	}

	@Test
	public void testAddThreeToGoal() {
		goalWhite.set(Checker.WHITE, 3);
		assertTrue(goalContains(Checker.WHITE, 3));
	}

	public boolean goalContains(Checker checker, int count) {
		return goalWhite.checker() == checker && goalWhite.count() == count;
	}
}