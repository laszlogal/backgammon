package com.backgammon.common.game.player.move;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.PlayerRelativeBoardImpl;
import com.backgammon.common.game.board.setup.StandardSetup;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.player.Forward;

public class EnterFromJailTest {
	private final Board board = new BoardFactory().build();
	private PlayerRelativeBoardImpl relativeBoard;

	@Before
	public void setUp() {
		relativeBoard = new PlayerRelativeBoardImpl(board, new Forward(), new StandardSetup());
		relativeBoard.jail().increment();
	}

	@Test
	public void enterToEmptyPoint() {
		EnterFromJail moves = new EnterFromJail(relativeBoard, new Roll(2, 0));
		MoveChecker enterMove = new MoveChecker(relativeBoard.jail(), relativeBoard.point(23));
		assertEquals(moves.getMoves().get(0), enterMove);
	}

	@Test
	public void enterToOwnPoint() {
		EnterFromJail moves = new EnterFromJail(relativeBoard, new Roll(1, 0));
		MoveChecker enterMove = new MoveChecker(relativeBoard.jail(), relativeBoard.point(24));
		assertEquals(moves.getMoves().get(0), enterMove);
	}

	@Test
	public void enterToOppositePoint() {
		relativeBoard.setOpposite(19, 5);
		EnterFromJail moves = new EnterFromJail(relativeBoard, new Roll(6, 0));
		assertTrue(moves.getMoves().isEmpty());
	}

	@Test
	public void enterAndHit() {
		relativeBoard.setOpposite(23, 1);
		EnterFromJail moves = new EnterFromJail(relativeBoard, new Roll(2, 0));
		MoveChecker enterMove = new MoveChecker(relativeBoard.jail(), relativeBoard.point(23));
		assertEquals(moves.getMoves().get(0), enterMove);
	}
}