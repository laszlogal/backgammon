package com.backgammon.common.game.factory;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.backgammon.common.game.board.point.Point;

public class PointFactoryTest {

	private final BoardFactory factory = new BoardFactory();

	@Test
	public void testEmptyPointsIndex() {
		indexShouldBeSet(PointFactory.empty());
	}

	@Test
	public void testStandardPointsIndex() {
		indexShouldBeSet(PointFactory.standard());
	}

	public void indexShouldBeSet(List<Point> points) {
		List<Integer> list = new ArrayList<>();
		points.forEach((point -> list.add(point.index())));
		assertEquals(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
				, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23), list);
	}
}