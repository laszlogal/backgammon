package com.backgammon.common.game.player.move;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class RollTest {

	private Roll roll;

	@Test
	public void testAscendingValues() {
		withValues(3, 4);
		orderShouldBe(4, 3);
	}

	@Test
	public void testDescendingValues() {
		withValues(6, 4);
		orderShouldBe(6, 4);
	}

	@Test
	public void testSum() {
		withValues(6, 4);
		assertEquals(10, roll.sum());
	}

	private void orderShouldBe(int value1, int value2) {
		assertEquals(Arrays.asList(value1, value2),
				Arrays.asList(roll.high(), roll.low()));
	}

	private void withValues(int value1, int value2) {
		roll = new Roll(value1, value2);
	}

	@Test
	public void testIsDoubled() {
		withValues(2, 2);
		assertTrue(roll.isDoubled());
	}

	@Test
	public void testIsNotDoubled() {
		withValues(1, 2);
		assertFalse(roll.isDoubled());
	}
}