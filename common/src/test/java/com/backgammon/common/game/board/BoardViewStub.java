package com.backgammon.common.game.board;

import com.backgammon.common.game.player.MoveFinishedHandler;
import com.backgammon.common.game.player.Turn;
import com.backgammon.common.game.player.move.MoveChecker;
import com.backgammon.common.game.player.move.Roll;

public class BoardViewStub implements BoardView {
	private Turn turn;


	@Override
	public void update() {
		// stub method
	}

	@Override
	public void resize(int width, int height) {
		// stub method
	}

	@Override
	public void animateMove(MoveChecker move, MoveFinishedHandler handler) {
		handler.onMoveFinished(move);
	}

	@Override
	public void setRoll(Roll roll) {
		// stub method
	}

	@Override
	public void drawWinner(String name) {

	}
}
