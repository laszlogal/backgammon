package com.backgammon.common.game.board;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.factory.PointFactory;

public class BoardImplTest {
	private final BoardFactory factory = new BoardFactory();
	private Board board;


	@Test
	public void testPoints() {
		List<Point> points = PointFactory.standard();
		board = new BoardImpl(points, Collections.emptyList(), null);
		assertEquals(points, board.points());
	}

	@Test
	public void testPointAbsolute() {
		List<Point> points = PointFactory.standard();
		board = new BoardImpl(points, Collections.emptyList(), null);
		List<Point> actual = new ArrayList<>();

		for (int number = 1; number < 25; number++) {
			actual.add(board.point(number));
		}

		assertEquals(actual, board.points());
	}
}