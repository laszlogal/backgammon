package com.backgammon.common.game.player.move;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.PlayerRelativeBoardImpl;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.board.setup.CheckerSetup;
import com.backgammon.common.game.board.setup.StandardSetup;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.factory.PointFactory;
import com.backgammon.common.game.player.Backward;

public class MoveValidatorTest {
	private MoveValidator validator;
	private PlayerRelativeBoardImpl playerBoard;

	@Test
	public void isValid24To23() {
		withStandardSetup();
		shouldBeValid(24, 23);
	}

	private void withStandardSetup() {
		withSetup(new StandardSetup());
	}

	private void withSetup(CheckerSetup setup) {
		Board board = new BoardFactory().withPoints(PointFactory.empty()).build();
		playerBoard = setup != null
				? new PlayerRelativeBoardImpl(board, new Backward(), setup)
				: new PlayerRelativeBoardImpl(board, new Backward());
		validator = new MoveValidator(playerBoard);
	}

	private void shouldBeValid(int from, int to) {
		assertTrue(isValid(from, to));
	}

	private boolean isValid(int from, int to) {
		return validator.isValid(from, to);
	}

	@Test
	public void isInvalid23To24() {
		withStandardSetup();
		shouldBeInvalid(23, 24);
	}

	private void shouldBeInvalid(int from, int to) {
		assertFalse(isValid(from, to));
	}

	@Test
	public void isInvalid24To19() {
		withStandardSetup();
		playerBoard.setOpposite(19, 5);
		shouldBeInvalid(24, 19);
	}

	@Test
	public void iBearOffShouldBeFalseWithStandardSetup() {
		withStandardSetup();
		assertFalse(validator.isBearOff());
	}

	@Test
	public void iBearOffShouldBeTrueWithAllHome() {
		withBearOffSetup();
		assertTrue(validator.isBearOff());
	}

	private void withBearOffSetup() {
		withEmptyBoard();
		playerBoard.point(2).set(Checker.BLACK, 5);
		playerBoard.point(3).set(Checker.BLACK, 5);
		playerBoard.point(4).set(Checker.BLACK, 5);
	}

	private void withEmptyBoard() {
		withSetup(null);
	}

	@Test
	public void iBearOffShouldBeFalseWithSomeHomeButNotAll() {
		withEmptyBoard();
		playerBoard.point(13).set(Checker.BLACK, 2);
		playerBoard.point(2).set(Checker.BLACK, 3);
		playerBoard.point(3).set(Checker.BLACK, 5);
		playerBoard.point(4).set(Checker.BLACK, 5);
		assertFalse(validator.isBearOff());
	}

	@Test
	public void iBearOffShouldBeTrueWithHomeAndInGoal() {
		withEmptyBoard();
		playerBoard.goal().set(Checker.BLACK, 4);
		playerBoard.point(2).set(Checker.BLACK, 1);
		playerBoard.point(3).set(Checker.BLACK, 5);
		playerBoard.point(4).set(Checker.BLACK, 5);
		assertTrue(validator.isBearOff());
	}

	@Test
	public void goalShouldBeValidDestinationOnBearOff() {
		withBearOffSetup();
		assertTrue(validator.isValid(playerBoard.point(6), playerBoard.goal()));
		assertTrue(validator.isValid(playerBoard.point(5), playerBoard.goal()));
		assertTrue(validator.isValid(playerBoard.point(4), playerBoard.goal()));
		assertTrue(validator.isValid(playerBoard.point(3), playerBoard.goal()));
		assertTrue(validator.isValid(playerBoard.point(2), playerBoard.goal()));
		assertTrue(validator.isValid(playerBoard.point(1), playerBoard.goal()));
	}

	@Test
	public void goalShouldNotBeValidDestinationNormally() {
		withBearOffLikeSetup();
		assertFalse(validator.isValid(playerBoard.point(6), playerBoard.goal()));
		assertFalse(validator.isValid(playerBoard.point(5), playerBoard.goal()));
		assertFalse(validator.isValid(playerBoard.point(4), playerBoard.goal()));
		assertFalse(validator.isValid(playerBoard.point(3), playerBoard.goal()));
		assertFalse(validator.isValid(playerBoard.point(2), playerBoard.goal()));
		assertFalse(validator.isValid(playerBoard.point(1), playerBoard.goal()));
	}


	private void withBearOffLikeSetup() {
		withEmptyBoard();
		playerBoard.point(2).set(Checker.BLACK, 4);
		playerBoard.point(3).set(Checker.BLACK, 5);
		playerBoard.point(4).set(Checker.BLACK, 5);
	}
}