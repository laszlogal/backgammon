package com.backgammon.common.game.player;

import static com.backgammon.common.clean.PointUtil.setWhite;
import static com.backgammon.common.clean.PointUtil.shouldBeEmpty;
import static com.backgammon.common.clean.PointUtil.shouldEqual;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.backgammon.common.clean.GameMasterStub;
import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.BoardViewStub;
import com.backgammon.common.game.board.PlayerRelativeBoardImpl;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.player.agent.ChooseFirstAgent;
import com.backgammon.common.game.player.move.Roll;

public class TurnTest {
	private final Board board = new BoardFactory().build();
	private Turn turn;
	private PlayerRelativeBoardImpl relativeBoard;

	private void withWhitePlayer(BackgammonAgent agent) {
		withPlayer(new Forward(), agent);
	}

	private void withPlayer(BoardAccess access, BackgammonAgent agent) {
		relativeBoard = new PlayerRelativeBoardImpl(board, access);
		turn = new Turn(relativeBoard, new BoardViewStub());
		PlayerImpl player = new PlayerImpl(turn, "testPlayer", agent);
		player.setGameMaster(new GameMasterStub());
	}

	@Test
	public void testMoveOneToEmpty() {
		withWhitePlayer(withChooseFirstAlgorithm());
		setWhite(relativeBoard, 24, 1);
		turn.start(new Roll(2, 0));
		shouldBeEmpty(relativeBoard, 24);
		shouldEqual(relativeBoard, 22, Checker.WHITE, 1);
	}

	private ChooseFirstAgent withChooseFirstAlgorithm() {
		return new ChooseFirstAgent();
	}

	@Test
	public void testHit() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.set(24, 1);
		relativeBoard.setOpposite(22, 1);
		turn.start(new Roll(2, 0));
		shouldBeEmpty(relativeBoard, 24);
		shouldEqual(relativeBoard, 22, Checker.WHITE, 1);
		assertEquals(1, relativeBoard.hits().count());
	}

	@Test
	public void testMoveToOwnPoint() {
		withWhitePlayer(withChooseFirstAlgorithm());
		setWhite(relativeBoard, 24, 1);
		setWhite(relativeBoard, 22, 1);
		turn.start(new Roll(2, 0));
		shouldBeEmpty(relativeBoard, 24);
		shouldEqual(relativeBoard, 22, Checker.WHITE, 2);
	}

	@Test
	public void testEnterBoard() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.jail().set(Checker.WHITE, 1);
		turn.start(new Roll(1, 0));
		assertEquals(0, relativeBoard.jail().count());
		assertEquals(0, relativeBoard.hits().count());
		shouldEqual(relativeBoard, 24, Checker.WHITE, 1);

	}

	@Test
	public void testEnterBoardWhenBothPlayerInJail() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.jail().set(Checker.WHITE, 1);
		relativeBoard.hits().set(Checker.BLACK, 1);
		turn.start(new Roll(1, 0));
		assertEquals(0, relativeBoard.jail().count());
		shouldEqual(relativeBoard, 24, Checker.WHITE, 1);
		withBlackPlayer(withChooseFirstAlgorithm());
		turn.start(new Roll(1, 0));
		assertEquals(0, relativeBoard.hits().count());
		shouldEqual(relativeBoard, 24, Checker.BLACK, 1);

	}

	@Test
	public void testHitWithEnteringBoard() {
		withBlackPlayer(withChooseFirstAlgorithm());
		relativeBoard.jail().set(Checker.BLACK, 1);
		relativeBoard.setOpposite(24, 1);
		turn.start(new Roll(1, 0));
		assertEquals(0, relativeBoard.jail().count());
		shouldEqual(relativeBoard, 24, Checker.BLACK, 1);
		assertEquals(1, relativeBoard.hits().count());
	}


	private void withBlackPlayer(BackgammonAgent agent) {
		withPlayer(new Backward(), agent);
	}

	@Test
	public void testEnterThreeBoard() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.jail().set(Checker.WHITE, 3);
		turn.start(new Roll(2, 1));
		assertEquals(1, relativeBoard.jail().count());
		shouldEqual(relativeBoard, 23, Checker.WHITE, 1);

	}

	@Test
	public void testBearOff() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.set(1, 15);
		turn.start(new Roll(3, 4));
		assertEquals(13, relativeBoard.point(1).count());
		assertEquals(2, relativeBoard.goal().count());
	}

	@Test
	public void testBearOffDouble() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.set(1, 10);
		relativeBoard.set(2, 3);
		relativeBoard.set(3, 2);
		turn.start(new Roll(3, 3));
		assertEquals(4, relativeBoard.goal().count());
	}

	@Test
	public void testBearOff2() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.set(1, 5);
		relativeBoard.set(2, 5);
		relativeBoard.set(3, 5);
		turn.start(new Roll(3, 4));
		assertEquals(2, relativeBoard.goal().count());
	}

	@Test
	public void testBearOff3() {
		withWhitePlayer(withChooseFirstAlgorithm());
		relativeBoard.set(1, 5);
		relativeBoard.goal().set(relativeBoard.access().checker(), 5);
		relativeBoard.set(3, 5);
		turn.start(new Roll(3, 4));
		assertEquals(7, relativeBoard.goal().count());
	}
}