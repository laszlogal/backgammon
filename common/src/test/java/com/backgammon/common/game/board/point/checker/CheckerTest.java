package com.backgammon.common.game.board.point.checker;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CheckerTest {
	@Test
	public void testWhiteOpposite() {
		assertEquals(Checker.BLACK, Checker.WHITE.opposite());
	}

	@Test
	public void testBlackOpposite() {
		assertEquals(Checker.WHITE, Checker.BLACK.opposite());
	}

	@Test
	public void testWhiteColor() {
		assertEquals("white", Checker.WHITE.getColor());
	}

	@Test
	public void testBlackColor() {
		assertEquals("black", Checker.BLACK.getColor());
	}

	@Test
	public void testWhiteBorderColor() {
		assertEquals("black", Checker.WHITE.getBorderColor());
	}

	@Test
	public void testBlackBorderColor() {
		assertEquals("#777777", Checker.BLACK.getBorderColor());
	}
}