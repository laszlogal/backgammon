package com.backgammon.common.game.board;

import static org.mockito.Mockito.*;

import org.junit.Test;

import com.backgammon.common.game.draw.DrawBoard;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.factory.PointFactory;

public class BoardViewTest {
	private final BoardFactory factory = new BoardFactory();
	private final Board board = factory.withPoints(PointFactory.standard()).build();
	private final DrawBoard drawBoard = mock(DrawBoard.class);
	private final BoardView view = new BoardViewImpl(board, drawBoard, null);

	@Test
	public void testUpdate() {
		view.update();
		backgroundShouldBeDrawn();
		goalsShouldBeDrawn();
		pointsShouldBeDrawn();
		barShouldBeDrawn();
	}

	@Test
	public void testResize() {
		view.resize(800, 600);
		verify(drawBoard, atLeastOnce()).resize(800, 600);
		backgroundShouldBeDrawn();
		goalsShouldBeDrawn();
		pointsShouldBeDrawn();
		barShouldBeDrawn();
	}

	public void barShouldBeDrawn() {
		verify(drawBoard).drawBar(board.bar());
	}

	public void pointsShouldBeDrawn() {
		verify(drawBoard, times(24)).drawPoint(any());
	}

	public void goalsShouldBeDrawn() {
		verify(drawBoard, times(2)).drawGoal(any());
	}


	public void backgroundShouldBeDrawn() {
		verify(drawBoard).drawBackground();
	}
}