package com.backgammon.common.game.player.move;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.backgammon.common.game.board.Board;
import com.backgammon.common.game.board.PlayerRelativeBoardImpl;
import com.backgammon.common.game.board.setup.BearOffSetup;
import com.backgammon.common.game.board.setup.CheckerSetup;
import com.backgammon.common.game.board.setup.StandardSetup;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.player.Forward;

public class MoveFromPointTest {

	private PlayerRelativeBoardImpl playerBoard;

	@Test
	public void testMovesFrom24WithRoll1And2() {
		withStandardSetup();
		MoveFromPoint move = new MoveFromPoint(playerBoard, 24, new Roll(1, 2));
		MoveChecker sumMove = newMove(24, 22);
		sumMove.makeNext(newMove(22, 21));
		assertThat(move.moves(), contains(newMove(24, 23), newMove(24, 22), sumMove));
	}

	@Test
	public void testMovesFrom24WithDouble2() {
		withStandardSetup();
		MoveFromPoint move = new MoveFromPoint(playerBoard, 24, new Roll(2, 2));
		assertThat(move.moves(), containsInAnyOrder(
				newMove(24, 22),
				newMove(24, 22).makeNext(newMove(22, 20)),
				newMove(24, 22).makeNext(newMove(22, 20).makeNext(newMove(20, 18))),
				newMove(24, 22).makeNext(newMove(22, 20).makeNext(newMove(20, 18).makeNext(newMove(18, 16)))
				)
				)
		);
	}

	@Test
	public void testMovesFrom24AndLowOccupiedWithRoll1And2() {
		withStandardSetup();
		playerBoard.setOpposite(23, 2);
		MoveFromPoint move = new MoveFromPoint(playerBoard, 24, new Roll(1, 2));
		assertThat(move.moves(), contains(newMove(24, 22), sumMove(24, 22, 21)));
	}

	@Test
	public void testMovesFrom24AndHighOccupiedWithRoll1And2() {
		withStandardSetup();
		playerBoard.setOpposite(22, 2);
		MoveFromPoint move = new MoveFromPoint(playerBoard, 24, new Roll(1, 2));
		assertThat(move.moves(), contains(newMove(24, 23), sumMove(24, 23, 21)));
	}

	private void withSetup(CheckerSetup setup) {
		Board board = new BoardFactory().build();
		playerBoard = new PlayerRelativeBoardImpl(board, new Forward(), setup);
	}

	@Test
	public void testMovesFrom24WithRoll1And2NoBetween() {
		withStandardSetup();
		playerBoard.setOpposite(23, 2);
		playerBoard.setOpposite(22, 2);
		playerBoard.setOpposite(21, 2);
		MoveFromPoint move = new MoveFromPoint(playerBoard, 24, new Roll(1, 2));
		assertTrue(move.isEmpty());
	}

	private void withStandardSetup() {
		withSetup(new StandardSetup());
	}

	private MoveChecker newMove(int from, int to) {
		return new MoveChecker(playerBoard.point(from), playerBoard.point(to));
	}

	@Test
	public void testHitMovesFrom24WithRoll1And2() {
		withStandardSetup();
		playerBoard.setOpposite(23, 1);
		MoveFromPoint move = new MoveFromPoint(playerBoard, 24, new Roll(1, 2));
		assertThat(move.moves(), containsInAnyOrder(newMove(24, 23), newMove(24, 22), sumMove(24, 23, 21))
		);
	}

	private MoveChecker sumMove(int from, int middle, int to) {
		MoveChecker move = newMove(from, middle);
		move.makeNext(newMove(middle, to));
		return move;
	}

	@Test
	public void testBearOffWithRoll5And6() {
		withSetup(new BearOffSetup());
		MoveFromPoint move = new MoveFromPoint(playerBoard, 4, new Roll(5, 6));
		assertThat(move.moves(), contains(moveToGoal(4), moveToGoal(4)));
	}

	@Test
	public void testBearOffWithRoll1And2() {
		withSetup(new BearOffSetup());
		MoveFromPoint move = new MoveFromPoint(playerBoard, 1, new Roll(1, 2));
		assertThat(move.moves(), contains(moveToGoal(1), moveToGoal(1)));
	}

	private MoveChecker moveToGoal(int number) {
		return new MoveChecker(playerBoard.point(number), playerBoard.goal());
	}
}