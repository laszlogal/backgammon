package com.backgammon.common.game.player.move;

import static com.backgammon.common.game.factory.Points.*;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import com.backgammon.common.game.board.point.Point;

public class MoveCheckerTest {

	@Test
	public void testMoveBetweenPointsNotBlot() {
		Point source = whitePoint(2);
		Point destination = emptyPoint(1);
		MoveChecker move = new MoveChecker(source, destination);
		assertFalse(move.isBlot());
	}

	@Test
	public void testMoveBetweenTwoOccupiedPointsNotBlot() {
		Point source = whitePoint(2);
		Point destination = whitePoint(1);
		MoveChecker move = new MoveChecker(source, destination);
		assertFalse(move.isBlot());
	}

	@Test
	public void testMoveBetweenPointsIsABlot() {
		Point source = whitePoint(2);
		Point destination = blackPoint(1);
		MoveChecker move = new MoveChecker(source, destination);
		assertTrue(move.isBlot());
	}

	@Test
	public void testMoveBetweenPoints() {
		Point source = whitePoint(2);
		Point destination = whitePoint(1);
		MoveChecker move = new MoveChecker(source, destination);
		move.execute();
		assertEquals(Arrays.asList(
				whitePoint(1), whitePoint(2)),
				Arrays.asList(source, destination));
	}

	@Test
	public void testMoveHitPoint() {
		Point source = whitePoint(2);
		Point destination = blackPoint(1);
		MoveChecker move = new MoveChecker(source, destination);
		move.execute();
		assertEquals(Arrays.asList(
				whitePoint(1), whitePoint(1)),
				Arrays.asList(source, destination));
	}

	@Test
	public void testMoveToAnEmptyPoint() {
		Point source = blackPoint(5);
		Point destination = emptyPoint(0);
		MoveChecker move = new MoveChecker(source, destination);
		move.execute();
		assertEquals(Arrays.asList(
				blackPoint(4), blackPoint(1)),
				Arrays.asList(source, destination));
	}

	@Test
	public void testEquals() {
		Point source = blackPoint(5);
		Point middle = blackPoint(5);
		Point destination = emptyPoint(0);
		MoveChecker move = new MoveChecker(source, middle);
		move.makeNext(new MoveChecker(middle, destination));
		assertEquals(new MoveChecker(blackPoint(5), blackPoint(5))
				.makeNext(new MoveChecker(blackPoint(5), destination)), move);
	}
}