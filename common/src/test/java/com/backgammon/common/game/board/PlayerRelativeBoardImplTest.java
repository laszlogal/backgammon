package com.backgammon.common.game.board;

import static com.backgammon.common.game.factory.Points.blackBar;
import static com.backgammon.common.game.factory.Points.whiteBar;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.backgammon.common.game.board.point.Bar;
import com.backgammon.common.game.board.point.BarImpl;
import com.backgammon.common.game.board.point.BarPoint;
import com.backgammon.common.game.board.point.Goal;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.Checker;
import com.backgammon.common.game.factory.BoardFactory;
import com.backgammon.common.game.factory.PointFactory;
import com.backgammon.common.game.player.Backward;
import com.backgammon.common.game.player.BoardAccess;
import com.backgammon.common.game.player.Forward;

public class PlayerRelativeBoardImplTest {
	private final BoardFactory factory = new BoardFactory();

	@Test
	public void testGoals() {
		List<Goal> goals = factory.createGoals();
		Board board = new BoardImpl(null, goals, null);
		PlayerRelativeBoard playerBoard = new PlayerRelativeBoardImpl(board, new Backward());
		assertThat(goals.get(1), is(playerBoard.goal()));
	}

	@Test
	public void testPointAccess() {
		PlayerRelativeBoard whiteBoard =
				withPlayerRelativeBoard(Checker.WHITE, new Forward(), null);
		PlayerRelativeBoard blackBoard =
				withPlayerRelativeBoard(Checker.WHITE, new Backward(), null);
		List<Point> forwardList = new ArrayList<>();
		List<Point> backwardList = new ArrayList<>();

		for (int number = 1; number < 25; number++) {
			forwardList.add(whiteBoard.point(number));
			backwardList.add(blackBoard.point(25 - number));
		}
		assertEquals(forwardList, backwardList);
	}

	public PlayerRelativeBoard withPlayerRelativeBoard(Checker checker, BoardAccess access, Bar bar) {
		List<Point> points = PointFactory.standard();
		Board board = new BoardImpl(points, Collections.emptyList(), bar);
		return new PlayerRelativeBoardImpl(board, access);
	}

	@Test
	public void testBar() {
		BarPoint whiteHits = whiteBar(1);
		BarPoint whiteJail = blackBar(2);
		Bar bar = new BarImpl(whiteHits, whiteJail);
		PlayerRelativeBoard playerBoard = withPlayerRelativeBoard(Checker.BLACK, null, bar);
		assertEquals(Arrays.asList(whiteHits, whiteJail),
				Arrays.asList(playerBoard.jail(),
						playerBoard.hits()));
	}

	@Test
	public void testWhiteOwnsWithStandardSetup() {
		PlayerRelativeBoard playerBoard = withPlayerRelativeBoard(Checker.BLACK, new Backward(), null);
		assertTrue(playerBoard.owns(1));
	}
}