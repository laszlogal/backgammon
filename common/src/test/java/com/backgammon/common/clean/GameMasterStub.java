package com.backgammon.common.clean;

import com.backgammon.common.game.GameMaster;

public class GameMasterStub implements GameMaster {
	@Override
	public void newGame() {

	}

	@Override
	public void endTurn() {

	}

	@Override
	public void onRollEnd(int diceValue1, int diceValue2) {

	}

	@Override
	public void says(String message) {

	}

	@Override
	public void update() {

	}
}
