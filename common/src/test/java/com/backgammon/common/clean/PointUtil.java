package com.backgammon.common.clean;


import static org.junit.Assert.assertTrue;

import com.backgammon.common.game.board.PlayerRelativeBoard;
import com.backgammon.common.game.board.point.Point;
import com.backgammon.common.game.board.point.checker.Checker;

public class PointUtil {

	public static void shouldEqual(PlayerRelativeBoard board, int number, Checker checker, int count) {
		Point point = board.point(number);
		assertTrue(point.checker() == checker && point.count() == count);
	}

	public static void setWhite(PlayerRelativeBoard board, int number, int count) {
		set(board, number, Checker.WHITE, count);
	}

	public static void setBlack(PlayerRelativeBoard board, int number, int count) {
		set(board, number, Checker.BLACK, count);
	}

	private static void set(PlayerRelativeBoard board, int number, Checker checker, int count) {
		board.point(number).set(checker, count);
	}

	public static void shouldBeEmpty(PlayerRelativeBoard board, int number) {
		Point point = board.point(number);
		assertTrue(point.checker() == Checker.NONE && point.count() == 0);
	}
}
